/*
 Navicat Premium Data Transfer

 Source Server         : 100-local
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 192.168.1.100:3306
 Source Schema         : wms

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 17/05/2018 04:56:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`  (
  `id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('00557b15-5271-11e8-9e27-0242ac110002', '魅族', NULL, '魅族公司', 0);
INSERT INTO `brand` VALUES ('038507b7-5271-11e8-9e27-0242ac110002', '苹果', NULL, '美国苹果公司', 0);
INSERT INTO `brand` VALUES ('067a6c40-5271-11e8-9e27-0242ac110002', '舒肤佳', NULL, '宝洁公司旗下公司', 0);
INSERT INTO `brand` VALUES ('09383a16-5271-11e8-9e27-0242ac110002', '海飞丝', NULL, '宝洁公司旗下品牌', 0);
INSERT INTO `brand` VALUES ('0c2a65ea-5271-11e8-9e27-0242ac110002', '清扬', NULL, '联合利华公司旗下品牌', 0);
INSERT INTO `brand` VALUES ('0d1cecaf-526f-11e8-9e27-0242ac110002', '公牛', NULL, '公牛电器', 0);
INSERT INTO `brand` VALUES ('0f8a57d3-5271-11e8-9e27-0242ac110002', '联想', NULL, '联想电脑', 0);
INSERT INTO `brand` VALUES ('12cf671a-5271-11e8-9e27-0242ac110002', '格力', NULL, '格力电器', 0);
INSERT INTO `brand` VALUES ('164eb7e8-5271-11e8-9e27-0242ac110002', '美的', NULL, '美的电器', 0);
INSERT INTO `brand` VALUES ('190ee7b1-5271-11e8-9e27-0242ac110002', '老板', NULL, '老板电器', 0);
INSERT INTO `brand` VALUES ('1bb70d32-5271-11e8-9e27-0242ac110002', '华硕', NULL, '华硕电脑', 0);
INSERT INTO `brand` VALUES ('1e6bc0d4-5271-11e8-9e27-0242ac110002', '索尼', NULL, '日本索尼公司', 0);
INSERT INTO `brand` VALUES ('8c481285-526f-11e8-9e27-0242ac110002', '百事可乐', NULL, '百事可乐饮料公司', 0);
INSERT INTO `brand` VALUES ('941a8be4-526f-11e8-9e27-0242ac110002', '子弹头', NULL, '子弹头电器', 0);
INSERT INTO `brand` VALUES ('9d13caca-526f-11e8-9e27-0242ac110002', '可口可乐', NULL, '可口可乐饮料公司', 0);
INSERT INTO `brand` VALUES ('ef65f7f1-5270-11e8-9e27-0242ac110002', '芬达', NULL, '可乐可乐公司旗下', 0);
INSERT INTO `brand` VALUES ('f394dde3-5270-11e8-9e27-0242ac110002', '美连达', NULL, '百事可乐旗下品牌', 0);
INSERT INTO `brand` VALUES ('f6584f3f-5270-11e8-9e27-0242ac110002', '乐事', NULL, '乐事食品公司', 0);
INSERT INTO `brand` VALUES ('fa37634b-5270-11e8-9e27-0242ac110002', '小米', NULL, '小米公司', 0);
INSERT INTO `brand` VALUES ('fcdac352-5270-11e8-9e27-0242ac110002', '华为', NULL, '华为公司', 0);

-- ----------------------------
-- Table structure for commodity
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity`  (
  `id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `brand_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kind_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `purchase_price` decimal(10, 2) NOT NULL,
  `sales_price` decimal(10, 2) NOT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity
-- ----------------------------
INSERT INTO `commodity` VALUES ('1', '可口可乐', '9d13caca-526f-11e8-9e27-0242ac110002', '5299a5e2-526e-11e8-9e27-0242ac110002', 2.00, 3.00, '瓶', '可口可乐', 0);
INSERT INTO `commodity` VALUES ('10', '海飞丝洗发水', '09383a16-5271-11e8-9e27-0242ac110002', '654950e6-526e-11e8-9e27-0242ac110002', 20.00, 25.00, '瓶', '海飞丝去屑洗发水', 0);
INSERT INTO `commodity` VALUES ('11', '舒肤佳香皂', '067a6c40-5271-11e8-9e27-0242ac110002', '654950e6-526e-11e8-9e27-0242ac110002', 8.00, 10.00, '块', '舒肤佳香皂', 0);
INSERT INTO `commodity` VALUES ('12', '清扬洗发水', '0c2a65ea-5271-11e8-9e27-0242ac110002', '654950e6-526e-11e8-9e27-0242ac110002', 20.00, 25.00, '瓶', '清扬男士去屑洗发水', 0);
INSERT INTO `commodity` VALUES ('13', '联想电脑', '0f8a57d3-5271-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 4999.00, 5499.00, '台', '联想电脑', 0);
INSERT INTO `commodity` VALUES ('14', '华硕笔记本', '1bb70d32-5271-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 6999.00, 7999.00, '台', '华硕笔记本电脑', 0);
INSERT INTO `commodity` VALUES ('15', '格力空调', '0f8a57d3-5271-11e8-9e27-0242ac110002', '57c0ce01-526e-11e8-9e27-0242ac110002', 5499.00, 5999.00, '台', '好空调，格力造', 0);
INSERT INTO `commodity` VALUES ('16', '美的洗衣机', '164eb7e8-5271-11e8-9e27-0242ac110002', '57c0ce01-526e-11e8-9e27-0242ac110002', 5499.00, 5999.00, '台', '美的洗衣机，智能家居', 0);
INSERT INTO `commodity` VALUES ('17', '老板油烟机', '190ee7b1-5271-11e8-9e27-0242ac110002', '57c0ce01-526e-11e8-9e27-0242ac110002', 4899.00, 4999.00, '台', '老板油烟机', 0);
INSERT INTO `commodity` VALUES ('18', '索尼音响', '1e6bc0d4-5271-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 1699.00, 1999.00, '台', '索尼影音', 0);
INSERT INTO `commodity` VALUES ('19', '公牛插座', '0d1cecaf-526f-11e8-9e27-0242ac110002', '7aef78e4-526e-11e8-9e27-0242ac110002', 60.00, 99.00, '个', '公牛插座', 0);
INSERT INTO `commodity` VALUES ('2', '百事可乐', '8c481285-526f-11e8-9e27-0242ac110002', '5299a5e2-526e-11e8-9e27-0242ac110002', 2.00, 3.00, '瓶', '可口可乐', 0);
INSERT INTO `commodity` VALUES ('20', '子弹头扳手', '941a8be4-526f-11e8-9e27-0242ac110002', '7aef78e4-526e-11e8-9e27-0242ac110002', 30.00, 49.00, '个', '子弹头扳手', 0);
INSERT INTO `commodity` VALUES ('21', '小米电视', 'fa37634b-5270-11e8-9e27-0242ac110002', '57c0ce01-526e-11e8-9e27-0242ac110002', 899.00, 1099.00, '台', '小米全高清互联网彩电', 0);
INSERT INTO `commodity` VALUES ('22', '小米扫地机器人', 'fa37634b-5270-11e8-9e27-0242ac110002', '57c0ce01-526e-11e8-9e27-0242ac110002', 1499.00, 1699.00, '台', '小米扫地机器人', 0);
INSERT INTO `commodity` VALUES ('3', '乐事薯片', 'f6584f3f-5270-11e8-9e27-0242ac110002', '5299a5e2-526e-11e8-9e27-0242ac110002', 4.00, 5.00, '包', '乐事薯片', 0);
INSERT INTO `commodity` VALUES ('4', '芬达饮料', 'ef65f7f1-5270-11e8-9e27-0242ac110002', '5299a5e2-526e-11e8-9e27-0242ac110002', 2.00, 3.00, '瓶', '芬达', 0);
INSERT INTO `commodity` VALUES ('4c6fc6ab-95f4-49f1-96e0-adfb8a20b451', '12', '00557b15-5271-11e8-9e27-0242ac110002', '5299a5e2-526e-11e8-9e27-0242ac110002', 12.00, 12.00, '12', NULL, 1);
INSERT INTO `commodity` VALUES ('5', '美连达', 'f394dde3-5270-11e8-9e27-0242ac110002', '5299a5e2-526e-11e8-9e27-0242ac110002', 2.00, 3.00, '瓶', '美连达', 0);
INSERT INTO `commodity` VALUES ('6', '小米手机', 'fa37634b-5270-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 1499.00, 1999.00, '部', '小米手机跑分第一', 0);
INSERT INTO `commodity` VALUES ('7', '华为手机', 'fcdac352-5270-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 1899.00, 2333.00, '部', '华为手机国产精品', 0);
INSERT INTO `commodity` VALUES ('8', '魅族手机', '00557b15-5271-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 1499.00, 1999.00, '部', '魅族手机，小清新', 0);
INSERT INTO `commodity` VALUES ('9', 'iPhone', '038507b7-5271-11e8-9e27-0242ac110002', '71f52084-526e-11e8-9e27-0242ac110002', 6799.00, 6899.00, '部', '苹果手机', 0);

-- ----------------------------
-- Table structure for inout
-- ----------------------------
DROP TABLE IF EXISTS `inout`;
CREATE TABLE `inout`  (
  `id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '1：入库2：出库',
  `commodity_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quantity` decimal(10, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inout
-- ----------------------------
INSERT INTO `inout` VALUES ('073fc2e4-53aa-11e8-8bef-0242ac130003', '2018-05-09 16:57:16', '出库', '1', 1.00);
INSERT INTO `inout` VALUES ('1ac55263-5276-11e8-9e27-0242ac110002', '2018-05-08 04:13:04', '入库', '1', 100.00);
INSERT INTO `inout` VALUES ('1ac5542e-5276-11e8-9e27-0242ac110002', '2018-05-08 04:13:04', '入库', '2', 100.00);
INSERT INTO `inout` VALUES ('1ac554c9-5276-11e8-9e27-0242ac110002', '2018-05-08 04:13:04', '入库', '4', 100.00);
INSERT INTO `inout` VALUES ('1ac55503-5276-11e8-9e27-0242ac110002', '2018-05-08 04:13:04', '入库', '5', 100.00);
INSERT INTO `inout` VALUES ('1ac55530-5276-11e8-9e27-0242ac110002', '2018-05-08 04:13:04', '入库', '3', 100.00);
INSERT INTO `inout` VALUES ('2bbffab4-53aa-11e8-8bef-0242ac130003', '2018-05-09 16:58:17', '出库', '1', 1.00);
INSERT INTO `inout` VALUES ('952bf485-53ab-11e8-8bef-0242ac130003', '2018-05-09 17:08:24', '出库', '1', 1.00);
INSERT INTO `inout` VALUES ('9fd2f023-53ab-11e8-8bef-0242ac130003', '2018-05-09 17:08:42', '出库', '1', 1.00);
INSERT INTO `inout` VALUES ('b31ba319-53ac-11e8-8bef-0242ac130003', '2018-05-09 17:16:23', '出库', '1', 1.00);
INSERT INTO `inout` VALUES ('bc038c85-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:34', '入库', '19', 50.00);
INSERT INTO `inout` VALUES ('bc038efd-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:34', '入库', '20', 50.00);
INSERT INTO `inout` VALUES ('bf5bb5e5-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '13', 20.00);
INSERT INTO `inout` VALUES ('bf5bb829-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '14', 20.00);
INSERT INTO `inout` VALUES ('bf5bb8b5-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '18', 20.00);
INSERT INTO `inout` VALUES ('bf5bb911-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '6', 20.00);
INSERT INTO `inout` VALUES ('bf5bb95d-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '7', 20.00);
INSERT INTO `inout` VALUES ('bf5bb9ae-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '8', 20.00);
INSERT INTO `inout` VALUES ('bf5bba04-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:40', '入库', '9', 20.00);
INSERT INTO `inout` VALUES ('c29e0835-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:45', '入库', '15', 20.00);
INSERT INTO `inout` VALUES ('c29e09fe-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:45', '入库', '16', 10.00);
INSERT INTO `inout` VALUES ('c29e0a49-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:45', '入库', '17', 12.00);
INSERT INTO `inout` VALUES ('c29e0b0e-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:45', '入库', '21', 20.00);
INSERT INTO `inout` VALUES ('c29e0b48-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:45', '入库', '22', 20.00);
INSERT INTO `inout` VALUES ('c537f272-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:50', '入库', '10', 100.00);
INSERT INTO `inout` VALUES ('c537f41f-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:50', '入库', '11', 200.00);
INSERT INTO `inout` VALUES ('c537f46e-5276-11e8-9e27-0242ac110002', '2018-05-08 04:17:50', '入库', '12', 100.00);
INSERT INTO `inout` VALUES ('c5b2667f-53ac-11e8-8bef-0242ac130003', '2018-05-09 17:16:55', '出库', '1', 1111111.00);
INSERT INTO `inout` VALUES ('d21eb733-53ac-11e8-8bef-0242ac130003', '2018-05-09 17:17:15', '出库', '1', 1111111.00);
INSERT INTO `inout` VALUES ('ddae334d-53ac-11e8-8bef-0242ac130003', '2018-05-09 17:17:35', '出库', '1', 1111111.00);
INSERT INTO `inout` VALUES ('e315eab9-5276-11e8-9e27-0242ac110002', '2018-05-08 04:18:40', '入库', '18', 50.00);
INSERT INTO `inout` VALUES ('ecd59d47-5276-11e8-9e27-0242ac110002', '2018-05-08 04:18:56', '出库', '18', 20.00);
INSERT INTO `inout` VALUES ('f709e5a0-53a9-11e8-8bef-0242ac130003', '2018-05-09 16:56:49', '出库', '1', 1.00);

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `commodity_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quantity` decimal(10, 2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item
-- ----------------------------
INSERT INTO `item` VALUES ('1', '1', 99.00);
INSERT INTO `item` VALUES ('10', '10', 100.00);
INSERT INTO `item` VALUES ('11', '11', 200.00);
INSERT INTO `item` VALUES ('12', '12', 100.00);
INSERT INTO `item` VALUES ('13', '13', 20.00);
INSERT INTO `item` VALUES ('14', '14', 20.00);
INSERT INTO `item` VALUES ('15', '15', 20.00);
INSERT INTO `item` VALUES ('16', '16', 10.00);
INSERT INTO `item` VALUES ('17', '17', 12.00);
INSERT INTO `item` VALUES ('18', '18', 50.00);
INSERT INTO `item` VALUES ('19', '19', 50.00);
INSERT INTO `item` VALUES ('2', '2', 100.00);
INSERT INTO `item` VALUES ('20', '20', 50.00);
INSERT INTO `item` VALUES ('21', '21', 20.00);
INSERT INTO `item` VALUES ('22', '22', 20.00);
INSERT INTO `item` VALUES ('3', '3', 100.00);
INSERT INTO `item` VALUES ('4', '4', 100.00);
INSERT INTO `item` VALUES ('43fb9732-53a7-11e8-8bef-0242ac130003', '4c6fc6ab-95f4-49f1-96e0-adfb8a20b451', 0.00);
INSERT INTO `item` VALUES ('5', '5', 100.00);
INSERT INTO `item` VALUES ('6', '6', 20.00);
INSERT INTO `item` VALUES ('7', '7', 20.00);
INSERT INTO `item` VALUES ('8', '8', 20.00);
INSERT INTO `item` VALUES ('9', '9', 20.00);

-- ----------------------------
-- Table structure for kind
-- ----------------------------
DROP TABLE IF EXISTS `kind`;
CREATE TABLE `kind`  (
  `id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kind
-- ----------------------------
INSERT INTO `kind` VALUES ('50bea360-5594-11e8-8bef-0242ac130003', '切尔奇无', NULL, NULL, 1);
INSERT INTO `kind` VALUES ('5299a5e2-526e-11e8-9e27-0242ac110002', '食物', NULL, '食物', 0);
INSERT INTO `kind` VALUES ('57c0ce01-526e-11e8-9e27-0242ac110002', '电器', NULL, '电器', 0);
INSERT INTO `kind` VALUES ('654950e6-526e-11e8-9e27-0242ac110002', '百货', NULL, '百货', 0);
INSERT INTO `kind` VALUES ('71f52084-526e-11e8-9e27-0242ac110002', '3C', NULL, '3C产品', 0);
INSERT INTO `kind` VALUES ('7aef78e4-526e-11e8-9e27-0242ac110002', '五金工具', NULL, '五金工具', 0);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `amount` decimal(10, 2) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('164d54cd-627e-4937-a836-25a3549808d3', '2018-05-08 04:17:35', 4500.00, '已通过', '入库');
INSERT INTO `order` VALUES ('291c6af4-32d3-4878-b07a-0816f8df4779', '2018-05-09 17:16:24', 2.00, '已通过', '出库');
INSERT INTO `order` VALUES ('2f78f497-857c-49a1-8738-d8b61f6479b0', '2018-05-08 04:17:40', 507860.00, '已通过', '入库');
INSERT INTO `order` VALUES ('5e1559e7-dd6d-48b1-86bc-fc301149e081', '2018-05-08 04:18:56', 33980.00, '已通过', '出库');
INSERT INTO `order` VALUES ('8415dc0b-14b3-4fe8-8331-d8c087b90670', '2018-05-08 04:18:09', 200.00, '未审核', '入库');
INSERT INTO `order` VALUES ('8b76c42a-e469-4d82-822b-dd21544f3a69', '2018-05-08 04:18:40', 84950.00, '已通过', '入库');
INSERT INTO `order` VALUES ('8e549357-a647-407d-a32f-f6d36c316a40', '2018-05-09 17:16:50', 2222222.00, '未审核', '出库');
INSERT INTO `order` VALUES ('a7f2e649-3984-4d1f-9a23-e63eb96f3c22', '2018-05-08 04:17:46', 271718.00, '已通过', '入库');
INSERT INTO `order` VALUES ('ad8af5c6-23d0-4392-992b-1eafdac1c394', '2018-05-08 04:13:04', 1200.00, '已通过', '入库');
INSERT INTO `order` VALUES ('cc5423b9-264c-4a33-a853-281856a0c860', '2018-05-08 04:17:50', 5600.00, '已通过', '入库');
INSERT INTO `order` VALUES ('ce35c99d-9377-4b99-8185-876fabff4e40', '2018-05-08 04:18:01', 200.00, '已拒绝', '入库');
INSERT INTO `order` VALUES ('ebe9f34d-a667-4827-8705-d2f86d157d03', '2018-05-08 04:24:08', 800.00, '未审核', '入库');
INSERT INTO `order` VALUES ('f8ca4f0c-bdd6-42dd-abe1-3a131bf89abc', '2018-05-09 16:40:28', 504.00, '未审核', '入库');

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`  (
  `commodity_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `quantity` decimal(10, 2) NULL DEFAULT NULL,
  `order_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES ('1', 100.00, 'ad8af5c6-23d0-4392-992b-1eafdac1c394');
INSERT INTO `order_item` VALUES ('2', 100.00, 'ad8af5c6-23d0-4392-992b-1eafdac1c394');
INSERT INTO `order_item` VALUES ('4', 100.00, 'ad8af5c6-23d0-4392-992b-1eafdac1c394');
INSERT INTO `order_item` VALUES ('5', 100.00, 'ad8af5c6-23d0-4392-992b-1eafdac1c394');
INSERT INTO `order_item` VALUES ('3', 100.00, 'ad8af5c6-23d0-4392-992b-1eafdac1c394');
INSERT INTO `order_item` VALUES ('15', 20.00, 'a7f2e649-3984-4d1f-9a23-e63eb96f3c22');
INSERT INTO `order_item` VALUES ('16', 10.00, 'a7f2e649-3984-4d1f-9a23-e63eb96f3c22');
INSERT INTO `order_item` VALUES ('17', 12.00, 'a7f2e649-3984-4d1f-9a23-e63eb96f3c22');
INSERT INTO `order_item` VALUES ('21', 20.00, 'a7f2e649-3984-4d1f-9a23-e63eb96f3c22');
INSERT INTO `order_item` VALUES ('22', 20.00, 'a7f2e649-3984-4d1f-9a23-e63eb96f3c22');
INSERT INTO `order_item` VALUES ('10', 100.00, 'cc5423b9-264c-4a33-a853-281856a0c860');
INSERT INTO `order_item` VALUES ('11', 200.00, 'cc5423b9-264c-4a33-a853-281856a0c860');
INSERT INTO `order_item` VALUES ('12', 100.00, 'cc5423b9-264c-4a33-a853-281856a0c860');
INSERT INTO `order_item` VALUES ('19', 50.00, '164d54cd-627e-4937-a836-25a3549808d3');
INSERT INTO `order_item` VALUES ('20', 50.00, '164d54cd-627e-4937-a836-25a3549808d3');
INSERT INTO `order_item` VALUES ('13', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('14', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('18', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('6', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('7', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('8', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('9', 20.00, '2f78f497-857c-49a1-8738-d8b61f6479b0');
INSERT INTO `order_item` VALUES ('4', 100.00, 'ce35c99d-9377-4b99-8185-876fabff4e40');
INSERT INTO `order_item` VALUES ('5', 100.00, '8415dc0b-14b3-4fe8-8331-d8c087b90670');
INSERT INTO `order_item` VALUES ('18', 50.00, '8b76c42a-e469-4d82-822b-dd21544f3a69');
INSERT INTO `order_item` VALUES ('18', 20.00, '5e1559e7-dd6d-48b1-86bc-fc301149e081');
INSERT INTO `order_item` VALUES ('1', 100.00, 'ebe9f34d-a667-4827-8705-d2f86d157d03');
INSERT INTO `order_item` VALUES ('2', 100.00, 'ebe9f34d-a667-4827-8705-d2f86d157d03');
INSERT INTO `order_item` VALUES ('4', 100.00, 'ebe9f34d-a667-4827-8705-d2f86d157d03');
INSERT INTO `order_item` VALUES ('5', 100.00, 'ebe9f34d-a667-4827-8705-d2f86d157d03');
INSERT INTO `order_item` VALUES ('12', 12.00, 'f8ca4f0c-bdd6-42dd-abe1-3a131bf89abc');
INSERT INTO `order_item` VALUES ('10', 12.00, 'f8ca4f0c-bdd6-42dd-abe1-3a131bf89abc');
INSERT INTO `order_item` VALUES ('1', 12.00, 'f8ca4f0c-bdd6-42dd-abe1-3a131bf89abc');
INSERT INTO `order_item` VALUES ('1', 1.00, '291c6af4-32d3-4878-b07a-0816f8df4779');
INSERT INTO `order_item` VALUES ('1', 1111111.00, '8e549357-a647-407d-a32f-f6d36c316a40');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (2, '系统设置菜单', 'SYS_SET_MENU', '系统设置菜单');
INSERT INTO `sys_permission` VALUES (3, '商品管理菜单', 'ITEM_MENU', '商品管理菜单');
INSERT INTO `sys_permission` VALUES (4, '库存动态菜单', 'IO_MENU', '库存动态菜单');
INSERT INTO `sys_permission` VALUES (5, '订单管理菜单', 'ORDER_MENU', '订单管理菜单');
INSERT INTO `sys_permission` VALUES (6, '采购订单管理', 'IN_ORDER', '采购订单管理');
INSERT INTO `sys_permission` VALUES (7, '出库订单管理', 'OUT_ORDER', '出库订单管理');
INSERT INTO `sys_permission` VALUES (8, '订单查询', 'ORDER_QUERY', '订单查询');
INSERT INTO `sys_permission` VALUES (9, '订单创建', 'ORDER_CREATE', '订单创建');
INSERT INTO `sys_permission` VALUES (10, '订单编辑', 'ORDER_EDIT', '订单编辑');
INSERT INTO `sys_permission` VALUES (11, '订单删除', 'ORDER_DELETE', '订单删除');
INSERT INTO `sys_permission` VALUES (12, '订单审核', 'ORDER_CHECK', '订单审核');
INSERT INTO `sys_permission` VALUES (13, '出入库查询', 'INOUT_QUERY', '出入库查询');
INSERT INTO `sys_permission` VALUES (14, '库存查询', 'INVENTORY_QUERY', '库存查询');
INSERT INTO `sys_permission` VALUES (15, '品牌管理', 'BRAND_MANAGE', '品牌管理');
INSERT INTO `sys_permission` VALUES (16, '品牌创建', 'BRAND_CREATE', '品牌创建');
INSERT INTO `sys_permission` VALUES (17, '品牌编辑', 'BRAND_EDIT', '品牌编辑');
INSERT INTO `sys_permission` VALUES (18, '品牌删除', 'BRAND_DELETE', '品牌删除');
INSERT INTO `sys_permission` VALUES (19, '品类管理', 'KIND_MANAGE', '品类管理');
INSERT INTO `sys_permission` VALUES (20, '品类创建', 'KIND_CREATE', '品类创建');
INSERT INTO `sys_permission` VALUES (21, '品类编辑', 'KIND_EDIT', '品类编辑');
INSERT INTO `sys_permission` VALUES (22, '品类删除', 'KIND_DELETE', '品类删除');
INSERT INTO `sys_permission` VALUES (23, '商品管理', 'ITEM_MANAGE', '商品管理');
INSERT INTO `sys_permission` VALUES (24, '商品创建', 'ITEM_CREATE', '商品创建');
INSERT INTO `sys_permission` VALUES (25, '商品编辑', 'ITEM_EDIT', '商品编辑');
INSERT INTO `sys_permission` VALUES (26, '商品删除', 'ITEM_DELETE', '商品删除');
INSERT INTO `sys_permission` VALUES (27, '角色创建', 'ROLE_CREATE', '角色创建');
INSERT INTO `sys_permission` VALUES (28, '角色编辑', 'ROLE_EDIT', '角色编辑');
INSERT INTO `sys_permission` VALUES (29, '角色删除', 'ROLE_DELETE', '角色删除');
INSERT INTO `sys_permission` VALUES (30, '用户创建', 'USER_ADD', '用户添加');
INSERT INTO `sys_permission` VALUES (31, '用户编辑', 'USER_EDIT', '用户编辑');
INSERT INTO `sys_permission` VALUES (32, '用户删除', 'USER_DELETE', '用户删除');
INSERT INTO `sys_permission` VALUES (33, '用户管理', 'USER_MANAGE', '用户管理');
INSERT INTO `sys_permission` VALUES (34, '角色管理', 'ROLE_MANAGE', '角色管理');
INSERT INTO `sys_permission` VALUES (35, '关联权限', 'ROLE_REF', '关联权限');
INSERT INTO `sys_permission` VALUES (36, '重置用户密码', 'USER_RESET', '重置用户密码');
INSERT INTO `sys_permission` VALUES (37, '权限管理', 'PERMIT_MANAGE', '权限管理');
INSERT INTO `sys_permission` VALUES (38, '关联权限', 'USER_REF', '关联权限');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '内置管理员', '内置管理员');
INSERT INTO `sys_role` VALUES (2, '管理员', '管理员');
INSERT INTO `sys_role` VALUES (3, '审核员', '拣货员');
INSERT INTO `sys_role` VALUES (15, NULL, '入库核验员');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  INDEX `FKomxrs8a388bknvhjokh440waq`(`permission_id`) USING BTREE,
  INDEX `FK9q28ewrhntqeipl1t04kh1be7`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1, 2);
INSERT INTO `sys_role_permission` VALUES (1, 3);
INSERT INTO `sys_role_permission` VALUES (1, 4);
INSERT INTO `sys_role_permission` VALUES (1, 5);
INSERT INTO `sys_role_permission` VALUES (1, 6);
INSERT INTO `sys_role_permission` VALUES (1, 7);
INSERT INTO `sys_role_permission` VALUES (1, 8);
INSERT INTO `sys_role_permission` VALUES (1, 9);
INSERT INTO `sys_role_permission` VALUES (1, 10);
INSERT INTO `sys_role_permission` VALUES (1, 11);
INSERT INTO `sys_role_permission` VALUES (1, 12);
INSERT INTO `sys_role_permission` VALUES (1, 13);
INSERT INTO `sys_role_permission` VALUES (1, 14);
INSERT INTO `sys_role_permission` VALUES (1, 15);
INSERT INTO `sys_role_permission` VALUES (1, 16);
INSERT INTO `sys_role_permission` VALUES (1, 17);
INSERT INTO `sys_role_permission` VALUES (1, 18);
INSERT INTO `sys_role_permission` VALUES (1, 19);
INSERT INTO `sys_role_permission` VALUES (1, 20);
INSERT INTO `sys_role_permission` VALUES (1, 21);
INSERT INTO `sys_role_permission` VALUES (1, 22);
INSERT INTO `sys_role_permission` VALUES (1, 23);
INSERT INTO `sys_role_permission` VALUES (1, 24);
INSERT INTO `sys_role_permission` VALUES (1, 25);
INSERT INTO `sys_role_permission` VALUES (1, 26);
INSERT INTO `sys_role_permission` VALUES (1, 27);
INSERT INTO `sys_role_permission` VALUES (1, 28);
INSERT INTO `sys_role_permission` VALUES (1, 29);
INSERT INTO `sys_role_permission` VALUES (1, 30);
INSERT INTO `sys_role_permission` VALUES (1, 31);
INSERT INTO `sys_role_permission` VALUES (1, 32);
INSERT INTO `sys_role_permission` VALUES (1, 33);
INSERT INTO `sys_role_permission` VALUES (1, 34);
INSERT INTO `sys_role_permission` VALUES (1, 35);
INSERT INTO `sys_role_permission` VALUES (1, 36);
INSERT INTO `sys_role_permission` VALUES (1, 37);
INSERT INTO `sys_role_permission` VALUES (1, 38);
INSERT INTO `sys_role_permission` VALUES (2, 10);
INSERT INTO `sys_role_permission` VALUES (2, 25);
INSERT INTO `sys_role_permission` VALUES (2, 29);
INSERT INTO `sys_role_permission` VALUES (2, 20);
INSERT INTO `sys_role_permission` VALUES (2, 18);
INSERT INTO `sys_role_permission` VALUES (2, 33);
INSERT INTO `sys_role_permission` VALUES (2, 4);
INSERT INTO `sys_role_permission` VALUES (2, 11);
INSERT INTO `sys_role_permission` VALUES (2, 24);
INSERT INTO `sys_role_permission` VALUES (2, 16);
INSERT INTO `sys_role_permission` VALUES (2, 7);
INSERT INTO `sys_role_permission` VALUES (2, 37);
INSERT INTO `sys_role_permission` VALUES (2, 15);
INSERT INTO `sys_role_permission` VALUES (2, 19);
INSERT INTO `sys_role_permission` VALUES (2, 21);
INSERT INTO `sys_role_permission` VALUES (2, 30);
INSERT INTO `sys_role_permission` VALUES (2, 2);
INSERT INTO `sys_role_permission` VALUES (2, 23);
INSERT INTO `sys_role_permission` VALUES (2, 6);
INSERT INTO `sys_role_permission` VALUES (2, 32);
INSERT INTO `sys_role_permission` VALUES (2, 8);
INSERT INTO `sys_role_permission` VALUES (2, 28);
INSERT INTO `sys_role_permission` VALUES (2, 38);
INSERT INTO `sys_role_permission` VALUES (2, 13);
INSERT INTO `sys_role_permission` VALUES (2, 31);
INSERT INTO `sys_role_permission` VALUES (2, 5);
INSERT INTO `sys_role_permission` VALUES (2, 27);
INSERT INTO `sys_role_permission` VALUES (2, 14);
INSERT INTO `sys_role_permission` VALUES (2, 12);
INSERT INTO `sys_role_permission` VALUES (2, 22);
INSERT INTO `sys_role_permission` VALUES (2, 26);
INSERT INTO `sys_role_permission` VALUES (2, 3);
INSERT INTO `sys_role_permission` VALUES (2, 17);
INSERT INTO `sys_role_permission` VALUES (2, 34);
INSERT INTO `sys_role_permission` VALUES (2, 36);
INSERT INTO `sys_role_permission` VALUES (2, 9);
INSERT INTO `sys_role_permission` VALUES (2, 35);
INSERT INTO `sys_role_permission` VALUES (15, 4);
INSERT INTO `sys_role_permission` VALUES (15, 6);
INSERT INTO `sys_role_permission` VALUES (15, 8);
INSERT INTO `sys_role_permission` VALUES (15, 13);
INSERT INTO `sys_role_permission` VALUES (15, 5);
INSERT INTO `sys_role_permission` VALUES (15, 14);
INSERT INTO `sys_role_permission` VALUES (15, 12);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `role_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  INDEX `FKgkmyslkrfeyn9ukmolvek8b8f`(`uid`) USING BTREE,
  INDEX `FKhh52n8vd4ny9ff4x9fb8v65qx`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 0);
INSERT INTO `sys_user_role` VALUES (2, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (15, 23);

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_f2ksd6h8hsjtd57ipfq9myr64`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES (0, '内置系统管理员', '21232f297a57a5a743894a0e4a801fc3', 'admin', '内置系统管理员');
INSERT INTO `user_info` VALUES (1, '张三', '01d7f40760960e7bd9443513f22ab9af', 'zhangsan', '高级管理员张三');
INSERT INTO `user_info` VALUES (2, '李四', 'dc3a8f1670d65bea69b7b65048a0ac40', 'lisi', '拣货员李四');
INSERT INTO `user_info` VALUES (3, '王五', '9f001e4166cf26bfbdd3b4f67d9ef617', 'wangwu', '审核员王五');
INSERT INTO `user_info` VALUES (23, '陈六', '0c7f02de5fde02f884944e30eb97c1c3', 'chenliu', NULL);

SET FOREIGN_KEY_CHECKS = 1;
