package com.sleepybit.wms.mapper;

import com.sleepybit.wms.entity.HomeInfo;
import com.sleepybit.wms.entity.Role;
import com.sleepybit.wms.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysMapper {
    HomeInfo getHomeInfo();

    Integer checkUser(User user);

    User getUser(User user);
}
