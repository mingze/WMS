package com.sleepybit.wms.mapper;

import com.sleepybit.wms.entity.Permit;
import com.sleepybit.wms.entity.Role;
import com.sleepybit.wms.entity.RolePermit;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RoleMapper {
    Integer insert(Role role);

    Integer update(Role role);

    Integer delete(Role role);

    List<Role> select(Role role);

    Integer addRolePermit(List<RolePermit> rolePermits);

    void deleteRolePermit(RolePermit rolePermit);

    List<Permit> getPermitByRole(Role role);
}
