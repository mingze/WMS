package com.sleepybit.wms.mapper;

import com.sleepybit.wms.entity.Permit;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PermitMapper {
    List<Permit> select(Permit permit);
}
