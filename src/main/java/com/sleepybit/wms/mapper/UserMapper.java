package com.sleepybit.wms.mapper;

import com.sleepybit.wms.entity.Role;
import com.sleepybit.wms.entity.RolePermit;
import com.sleepybit.wms.entity.User;
import com.sleepybit.wms.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    List<User> select(User user);

    Integer insert(User user);

    Integer update(User user);

    Integer delete(User user);

    List<Role> getRoleByUser(User user);

    Integer addUserRole(List<UserRole> userRoles);

    void deleteUserRole(UserRole userRole);
}
