package com.sleepybit.wms.mapper;

import com.sleepybit.wms.dto.OrderItemDTO;
import com.sleepybit.wms.entity.Order;
import com.sleepybit.wms.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderItemMapper {
    Integer insert(OrderItemDTO orderItemDTO);

    List<Order> select(Order order);

    List<OrderItem> getOrderItemByOrderId(String orderId);

    Integer deleteByOrderId(String orderId);
}
