package com.sleepybit.wms.mapper;

import com.sleepybit.wms.bo.CommodityBO;
import com.sleepybit.wms.entity.Commodity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CommodityMapper {
    List<Commodity> listAll();

    List<Commodity> list();

    Integer insert(Commodity commodity);

    Integer update(Commodity commodity);

    Integer delete(List<Commodity> commodities);

    List<Commodity> select(CommodityBO commodityBO);
}
