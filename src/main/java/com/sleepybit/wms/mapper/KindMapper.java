package com.sleepybit.wms.mapper;

import com.sleepybit.wms.bo.KindBO;
import com.sleepybit.wms.entity.Kind;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface KindMapper {
    List<Kind> listAll();

    List<Kind> list();

    Integer insert(Kind kind);

    Integer update(Kind kind);

    Integer delete(List<Kind> kinds);

    List<Kind> select(KindBO kindBO);
}
