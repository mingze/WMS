package com.sleepybit.wms.mapper;

import com.sleepybit.wms.bo.BrandBO;
import com.sleepybit.wms.entity.Brand;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BrandMapper {
    List<Brand> listAll();

    List<Brand> getBrandByName(Brand brand);

    Integer insert(Brand brand);

    Integer update(Brand brand);

    Integer delete(List<Brand> brands);

    List<Brand> select(BrandBO brandBO);
}
