package com.sleepybit.wms.mapper;

import com.sleepybit.wms.bo.ItemBO;
import com.sleepybit.wms.dto.ItemDTO;
import com.sleepybit.wms.entity.Item;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ItemMapper {
    List<ItemDTO> select(ItemBO itemBO);

    Integer insert(Item item);

    Integer update(Item item);

    Integer delete(Item item);

    List<ItemDTO> getItem();
}
