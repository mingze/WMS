package com.sleepybit.wms.mapper;

import com.sleepybit.wms.bo.InOutBO;
import com.sleepybit.wms.dto.InOutDTO;
import com.sleepybit.wms.entity.InOut;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface InOutMapper {
    List<InOutDTO> getInOutList();

    Integer insert(List<InOut> inOuts);

    List<InOutDTO> select(InOutBO inOutBO);
}
