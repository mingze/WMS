package com.sleepybit.wms.mapper;

import com.sleepybit.wms.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderMapper {
    List<Order> select(Order order);

    Integer insert(Order order);

    Integer update(Order order);

    Integer refuse(String orderId);

    Integer pass(String orderId);

    Integer delete(String orderId);
}
