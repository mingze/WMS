package com.sleepybit.wms.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class InOutDTO {
    private String id;
    private String commodityName;
    private String brandName;
    private String kindName;
    private BigDecimal quantity;
    private String time;
    private String type;

    public InOutDTO(String id, String commodityName, String brandName, String kindName, BigDecimal quantity, String time, String type) {
        this.id = id;
        this.commodityName = commodityName;
        this.brandName = brandName;
        this.kindName = kindName;
        this.quantity = quantity;
        this.time = time;
        this.type = type;
    }

    public InOutDTO() {
    }

    @Override
    public String toString() {
        return "InOutDTO{" +
                "id='" + id + '\'' +
                ", commodityName='" + commodityName + '\'' +
                ", brandName='" + brandName + '\'' +
                ", kindName='" + kindName + '\'' +
                ", quantity=" + quantity +
                ", time=" + time +
                ", type='" + type + '\'' +
                '}';
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

