package com.sleepybit.wms.dto;

import java.math.BigDecimal;

public class ItemDTO {
    private String id;
    private String commodityName;
    private String commodityId;
    private String brandName;
    private String kindName;
    private BigDecimal quantity;

    public ItemDTO() {
    }

    public ItemDTO(String id, String commodityName, String brandName, String kindName, BigDecimal quantity) {
        this.id = id;
        this.commodityName = commodityName;
        this.brandName = brandName;
        this.kindName = kindName;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ItemDTO{" +
                "id='" + id + '\'' +
                ", commodityName='" + commodityName + '\'' +
                ", commodityId='" + commodityId + '\'' +
                ", brandName='" + brandName + '\'' +
                ", kindName='" + kindName + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
