package com.sleepybit.wms.dto;

import java.math.BigDecimal;

public class OrderItemDTO {
    private String commodityId;
    private BigDecimal quantity;
    private String orderId;

    public OrderItemDTO() {
    }

    public OrderItemDTO(String commodityId, BigDecimal quantity, String orderId) {
        this.commodityId = commodityId;
        this.quantity = quantity;
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderItemDTO{" +
                "commodityId='" + commodityId + '\'' +
                ", quantity=" + quantity +
                ", orderId='" + orderId + '\'' +
                '}';
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
