package com.sleepybit.wms.entity;

import java.math.BigDecimal;

public class Item {
    private String id;
    private String commodityId;
    private BigDecimal quantity;

    public Item(String id, String commodityId, BigDecimal quantity) {
        this.id = id;
        this.commodityId = commodityId;
        this.quantity = quantity;
    }

    public Item() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }


    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", commodityId='" + commodityId + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
