package com.sleepybit.wms.entity;

import java.math.BigDecimal;

public class OrderItem {
    private String commodityId;
    private BigDecimal quantity;
    private String orderId;

    public OrderItem(String commodityId, BigDecimal quantity, String orderId) {
        this.commodityId = commodityId;
        this.quantity = quantity;
        this.orderId = orderId;
    }

    public OrderItem() {
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "commodityId='" + commodityId + '\'' +
                ", quantity=" + quantity +
                ", orderId='" + orderId + '\'' +
                '}';
    }
}
