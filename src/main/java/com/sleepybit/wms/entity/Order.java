package com.sleepybit.wms.entity;

import java.math.BigDecimal;

public class Order {
    private String id;
    private String time;
    private BigDecimal amount;
    private String status;
    private String type;

    public Order(String id, String time, BigDecimal amount, String status, String type) {
        this.id = id;
        this.time = time;
        this.amount = amount;
        this.status = status;
        this.type = type;
    }

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", time='" + time + '\'' +
                ", amount=" + amount +
                ", status='" + status + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
