package com.sleepybit.wms.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class InOut {
    private String id;
    private String time;
    private String type;
    private String commodityId;
    private BigDecimal quantity;

    public InOut() {
    }

    public InOut(String id, String time, String type, String commodityId, BigDecimal quantity) {
        this.id = id;
        this.time = time;
        this.type = type;
        this.commodityId = commodityId;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "InOut{" +
                "id='" + id + '\'' +
                ", time='" + time + '\'' +
                ", type='" + type + '\'' +
                ", commodityId='" + commodityId + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
