package com.sleepybit.wms.entity;

import java.math.BigDecimal;

public class Commodity {
    private String id;
    private String name;
    private String brandId;
    private String kindId;
    private BigDecimal purchasePrice;
    private BigDecimal salesPrice;
    private String unit;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Commodity() {
    }

    public Commodity(String id, String name, String brandId, String kindId, BigDecimal purchasePrice, BigDecimal salesPrice, String unit) {
        this.id = id;
        this.name = name;
        this.brandId = brandId;
        this.kindId = kindId;
        this.purchasePrice = purchasePrice;
        this.salesPrice = salesPrice;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", brandId='" + brandId + '\'' +
                ", kindId='" + kindId + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", salesPrice=" + salesPrice +
                ", unit='" + unit + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getKindId() {
        return kindId;
    }

    public void setKindId(String kindId) {
        this.kindId = kindId;
    }

    public BigDecimal getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(BigDecimal purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(BigDecimal salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
