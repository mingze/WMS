package com.sleepybit.wms.bo;

public class InOutBO {

    private String id;
    private String commodityName;
    private String brandName;
    private String kindName;
    private String type;

    public InOutBO() {
    }

    public InOutBO(String id, String commodityName, String brandName, String kindName, String type) {
        this.id = id;
        this.commodityName = commodityName;
        this.brandName = brandName;
        this.kindName = kindName;
        this.type = type;
    }

    @Override
    public String toString() {
        return "InOutBO{" +
                "id='" + id + '\'' +
                ", commodityName='" + commodityName + '\'' +
                ", brandName='" + brandName + '\'' +
                ", kindName='" + kindName + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
