package com.sleepybit.wms.bo;

import com.sleepybit.wms.entity.Commodity;

import java.math.BigDecimal;
import java.util.List;

public class OrderItemBO {
    private Commodity commodity;
    private BigDecimal quantity;
    private String commodityId;
    private String orderId;

    public OrderItemBO(Commodity commodity, BigDecimal quantity, String commodityId, String orderId) {
        this.commodity = commodity;
        this.quantity = quantity;
        this.commodityId = commodityId;
        this.orderId = orderId;
    }

    public OrderItemBO() {
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrderItemBO{" +
                "commodity=" + commodity +
                ", quantity=" + quantity +
                ", commodityId='" + commodityId + '\'' +
                ", orderId='" + orderId + '\'' +
                '}';
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
