package com.sleepybit.wms.bo;

public class KindBO {
    private String name;

    public KindBO() {
    }

    public KindBO(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "KindBO{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
