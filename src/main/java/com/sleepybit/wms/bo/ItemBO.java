package com.sleepybit.wms.bo;

public class ItemBO {
    private String commodityName;
    private String brandName;
    private String kindName;

    public ItemBO() {
    }

    public ItemBO(String commodityName, String brandName, String kindName) {
        this.commodityName = commodityName;
        this.brandName = brandName;
        this.kindName = kindName;
    }

    @Override
    public String toString() {
        return "ItemBO{" +
                "commodityName='" + commodityName + '\'' +
                ", brandName='" + brandName + '\'' +
                ", kindName='" + kindName + '\'' +
                '}';
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }
}
