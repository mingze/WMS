package com.sleepybit.wms.bo;

public class CommodityBO {

    private String id;
    private String name;
    private String kindName;
    private String brandName;

    public CommodityBO() {
    }

    public CommodityBO(String id, String name, String kindName, String brandName) {
        this.id = id;
        this.name = name;
        this.kindName = kindName;
        this.brandName = brandName;
    }

    @Override
    public String toString() {
        return "CommodityBO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", kindName='" + kindName + '\'' +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
