package com.sleepybit.wms.bo;

public class BrandBO {
    private String name;

    public BrandBO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BrandBO() {
    }

    @Override
    public String toString() {
        return "BrandBO{" +
                "name='" + name + '\'' +
                '}';
    }
}
