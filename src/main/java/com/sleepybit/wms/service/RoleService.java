package com.sleepybit.wms.service;

import com.sleepybit.wms.entity.Role;
import com.sleepybit.wms.entity.RolePermit;
import com.sleepybit.wms.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RoleMapper roleMapper;

    public List<Role> queryRole(Role role) {
        return roleMapper.select(role);
    }

    public Integer addRole(Role role) {
        return roleMapper.insert(role);
    }

    public Integer editRole(Role role) {
        return roleMapper.update(role);
    }

    public Integer deleteRole(Role role) {
        return roleMapper.delete(role);
    }

    public Integer rolePermit(List<RolePermit> rolePermits) {
        roleMapper.deleteRolePermit(rolePermits.get(0));
        return roleMapper.addRolePermit(rolePermits);
    }
}
