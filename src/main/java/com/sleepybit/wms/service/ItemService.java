package com.sleepybit.wms.service;

import com.sleepybit.wms.bo.InOutBO;
import com.sleepybit.wms.bo.ItemBO;
import com.sleepybit.wms.dto.InOutDTO;
import com.sleepybit.wms.dto.ItemDTO;
import com.sleepybit.wms.entity.Item;
import com.sleepybit.wms.mapper.InOutMapper;
import com.sleepybit.wms.mapper.ItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {


    @Autowired
    private ItemMapper itemMapper;

    public List<ItemDTO> listItem(ItemBO itemBO) {
        return itemMapper.select(itemBO);
    }

    @Autowired
    private InOutMapper inOutMapper;

    public List<InOutDTO> getInOutList(InOutBO inOutBO) {
        return inOutMapper.select(inOutBO);
    }
}
