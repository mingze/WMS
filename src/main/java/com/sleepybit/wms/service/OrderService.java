package com.sleepybit.wms.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sleepybit.wms.bo.ItemBO;
import com.sleepybit.wms.bo.OrderItemBO;
import com.sleepybit.wms.dto.ItemDTO;
import com.sleepybit.wms.dto.OrderItemDTO;
import com.sleepybit.wms.entity.*;
import com.sleepybit.wms.mapper.InOutMapper;
import com.sleepybit.wms.mapper.ItemMapper;
import com.sleepybit.wms.mapper.OrderItemMapper;
import com.sleepybit.wms.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Service
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private InOutMapper inOutMapper;

    @Autowired
    private ItemMapper itemMapper;


    private BigDecimal insertOrderItem(String orderId, List<OrderItemBO> orderItemBOList) {
        BigDecimal amount = BigDecimal.ZERO;
        for (OrderItemBO item : orderItemBOList) {
            Commodity commodity = item.getCommodity();
            BigDecimal quantity = item.getQuantity();
            BigDecimal purchasePrice = commodity.getPurchasePrice();
            amount = amount.add(quantity.multiply(purchasePrice));
            String commodityId = commodity.getId();
            OrderItemDTO orderItemDTO = new OrderItemDTO(commodityId, quantity, orderId);
            orderItemMapper.insert(orderItemDTO);
        }
        return amount;
    }

    private Integer editOrder(List<OrderItemBO> orderItemBOList) {
        String orderId = orderItemBOList.get(0).getOrderId();
        orderItemMapper.deleteByOrderId(orderId);
        BigDecimal amount = insertOrderItem(orderId, orderItemBOList);
        Order order = new Order();
        order.setId(orderId);
        order.setAmount(amount);
        return orderMapper.update(order);
    }

    public Integer insertPurchaseOrder(List<OrderItemBO> orderItemBOList) {
        String orderId = UUID.randomUUID().toString();
        BigDecimal amount = insertOrderItem(orderId, orderItemBOList);
        Order order = new Order();
        order.setId(orderId);
        order.setType("入库");
        order.setStatus("未审核");
        order.setAmount(amount);
        return orderMapper.insert(order);
    }

    public List<Order> getPurchaseOrder(Order order) {
        order.setType("入库");
        return orderMapper.select(order);
    }

    public Integer insertSalesOrder(List<OrderItemBO> orderItemBOList) {

        //先判断仓库中库存是否满足


        String orderId = UUID.randomUUID().toString();
        BigDecimal amount = insertOrderItem(orderId, orderItemBOList);
        Order order = new Order();
        order.setId(orderId);
        order.setType("出库");
        order.setStatus("未审核");
        order.setAmount(amount);
        return orderMapper.insert(order);
    }

    public List<Order> getSalesOrder(Order order) {
        order.setType("出库");
        return orderMapper.select(order);
    }

    public List<OrderItem> getOrderItemByOrderId(String orderId) {
        return orderItemMapper.getOrderItemByOrderId(orderId);
    }

    public Integer editPurchaseOrder(List<OrderItemBO> orderItemBOList) {
        return editOrder(orderItemBOList);
    }

    public Integer editSalesOrder(List<OrderItemBO> orderItemBOList) {
        return editOrder(orderItemBOList);
    }


    public Integer refuseOrder(String orderId) {
        return orderMapper.refuse(orderId);
    }

    public Integer passOrder(String orderId) {
        List<OrderItem> orderItems = orderItemMapper.getOrderItemByOrderId(orderId);

        //获得订单类型
        Order order = new Order();
        order.setId(orderId);
        List<Order> orders = orderMapper.select(order);
        order = orders.get(0);
        String type = order.getType();
        Map<String, BigDecimal> itemMap = new HashMap();
        //将订单物品转化为仓库商品
        List<InOut> inOuts = new ArrayList<>();
        for (OrderItem item : orderItems) {
            if (itemMap.get(item.getCommodityId()) == null) {
                itemMap.put(item.getCommodityId(), item.getQuantity());
            } else {
                BigDecimal quantity = itemMap.get(item.getCommodityId()).add(item.getQuantity());
                itemMap.put(item.getCommodityId(), quantity);
            }
            InOut inOut = new InOut();
            inOut.setCommodityId(item.getCommodityId());
            inOut.setQuantity(item.getQuantity());
            inOut.setType(type);
            inOuts.add(inOut);
        }
        inOutMapper.insert(inOuts);
        //整合库存信息
        List<Item> items = new ArrayList<>();
        if (type.equals("入库")) {
            for (String key : itemMap.keySet()) {
                Item item = new Item();
                item.setCommodityId(key);
                item.setQuantity(itemMap.get(key));
                items.add(item);
            }
        } else if (type.equals("出库")) {
            Map<String, BigDecimal> itemMapI = new HashMap();
            List<ItemDTO> itemDTOS = itemMapper.getItem();
            itemDTOS.forEach(temp -> {
                itemMapI.put(temp.getCommodityId(), temp.getQuantity());
            });
            for (String key : itemMap.keySet()) {
                Item item = new Item();
                item.setCommodityId(key);
                BigDecimal tempAmount = itemMap.get(key);
                if (tempAmount.compareTo(itemMapI.get(key)) > 0) {
                    return -2;
                }
                item.setQuantity(BigDecimal.ZERO.subtract(tempAmount));
                items.add(item);
            }
        }
        for (Item item : items) {
            itemMapper.update(item);
        }
        return orderMapper.pass(orderId);
    }

    public Integer deleteOrder(String orderId) {
        orderItemMapper.deleteByOrderId(orderId);
        return orderMapper.delete(orderId);
    }
}
