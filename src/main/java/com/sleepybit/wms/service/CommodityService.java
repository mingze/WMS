package com.sleepybit.wms.service;

import com.sleepybit.wms.bo.BrandBO;
import com.sleepybit.wms.bo.CommodityBO;
import com.sleepybit.wms.bo.KindBO;
import com.sleepybit.wms.entity.Brand;
import com.sleepybit.wms.entity.Commodity;
import com.sleepybit.wms.entity.Item;
import com.sleepybit.wms.entity.Kind;
import com.sleepybit.wms.mapper.BrandMapper;
import com.sleepybit.wms.mapper.CommodityMapper;
import com.sleepybit.wms.mapper.ItemMapper;
import com.sleepybit.wms.mapper.KindMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Service
public class CommodityService {
    @Autowired
    private BrandMapper brandMapper;
    @Autowired
    KindMapper kindMapper;
    @Autowired
    private CommodityMapper commodityMapper;

    @Autowired
    private ItemMapper itemMapper;

    public List<Brand> getBrandList(BrandBO brandBO) {
        return brandMapper.select(brandBO);
    }

    public Integer insertBrand(Brand brand) {
        return brandMapper.insert(brand);
    }

    public Integer updateBrand(Brand brand) {
        return brandMapper.update(brand);
    }

    public Integer deleteBrand(List<Brand> brands) {

        return brandMapper.delete(brands);
    }

    public List<Kind> getKindList(KindBO kindBO) {
        return kindMapper.select(kindBO);
    }

    public Integer insertKind(Kind kind) {
        return kindMapper.insert(kind);
    }

    public Integer updateKind(Kind kind) {
        return kindMapper.update(kind);
    }

    public Integer deleteKind(List<Kind> kinds) {
        return kindMapper.delete(kinds);
    }

    public List<Commodity> getCommodityList(CommodityBO commodityBO) {
        return commodityMapper.select(commodityBO);
    }

    public Integer insertCommodity(Commodity commodity) {
        Item item = new Item();
        String uuid = UUID.randomUUID().toString();
        commodity.setId(uuid);
        item.setCommodityId(uuid);
        item.setQuantity(BigDecimal.ZERO);
        itemMapper.insert(item);
        return commodityMapper.insert(commodity);
    }

    public Integer updateCommodity(Commodity commodity) {
        return commodityMapper.update(commodity);
    }

    public Integer deleteCommodity(List<Commodity> commodities) {
        return commodityMapper.delete(commodities);
    }

    public List<Brand> getBrandByName(Brand brand) {
        return brandMapper.getBrandByName(brand);
    }
}
