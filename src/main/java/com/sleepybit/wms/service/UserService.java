package com.sleepybit.wms.service;

import com.sleepybit.wms.entity.RolePermit;
import com.sleepybit.wms.entity.User;
import com.sleepybit.wms.entity.UserRole;
import com.sleepybit.wms.mapper.UserMapper;
import com.sleepybit.wms.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public List<User> queryUser(User user) {
        return userMapper.select(user);
    }

    public Integer addUser(User user) throws NoSuchAlgorithmException {
        user.setPassword(MD5Util.crypt(user.getPassword()));
        return userMapper.insert(user);
    }

    public Integer editUser(User user) {
        return userMapper.update(user);
    }

    public Integer deleteUser(User user) {
        return userMapper.delete(user);
    }

    public Integer userRole(List<UserRole> userRoles) {
        userMapper.deleteUserRole(userRoles.get(0));
        return userMapper.addUserRole(userRoles);
    }
}
