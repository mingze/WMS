package com.sleepybit.wms.controller;

import com.sleepybit.wms.bo.OrderItemBO;
import com.sleepybit.wms.entity.Order;
import com.sleepybit.wms.entity.OrderItem;
import com.sleepybit.wms.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/order/createPurchaseOrder", method = RequestMethod.POST)
    public Integer createPurchaseOrder(@RequestBody List<OrderItemBO> orderItemBOList) {
        return orderService.insertPurchaseOrder(orderItemBOList);
    }

    @RequestMapping(value = "/order/getPurchaseOrderList", method = RequestMethod.POST)
    public List<Order> getPurchaseOrderList(@RequestBody Order order) {
        return orderService.getPurchaseOrder(order);
    }

    @RequestMapping(value = "/order/editPurchaseOrder", method = RequestMethod.POST)
    public Integer editPurchaseOrder(@RequestBody List<OrderItemBO> orderItemBOList) {
        return orderService.editPurchaseOrder(orderItemBOList);
    }

    @RequestMapping(value = "/order/createSalesOrder", method = RequestMethod.POST)
    public Integer createSalesOrder(@RequestBody List<OrderItemBO> orderItemBOList) {
        return orderService.insertSalesOrder(orderItemBOList);
    }

    @RequestMapping(value = "/order/getSalesOrderList", method = RequestMethod.POST)
    public List<Order> getSalesOrderList(@RequestBody Order order) {
        return orderService.getSalesOrder(order);
    }

    @RequestMapping(value = "/order/editSalesOrder", method = RequestMethod.POST)
    public Integer editSalesOrder(@RequestBody List<OrderItemBO> orderItemBOList) {
        return orderService.editSalesOrder(orderItemBOList);
    }

    @RequestMapping(value = "/order/getOrderItem", method = RequestMethod.POST)
    public List<OrderItem> getOrderItemByOrderId(@RequestBody String orderId) {
        return orderService.getOrderItemByOrderId(orderId);
    }

    @RequestMapping(value = "/order/refuseOrder", method = RequestMethod.POST)
    public Integer refuseOrder(@RequestBody String orderId) {
        return orderService.refuseOrder(orderId);
    }

    @RequestMapping(value = "/order/passOrder", method = RequestMethod.POST)
    public Integer passOrder(@RequestBody String orderId) {
        return orderService.passOrder(orderId);
    }

    @RequestMapping(value = "/order/deleteOrder", method = RequestMethod.POST)
    public Integer deleteOrder(@RequestBody String orderId) {
        return orderService.deleteOrder(orderId);
    }
}
