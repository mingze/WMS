package com.sleepybit.wms.controller;

import com.sleepybit.wms.entity.RolePermit;
import com.sleepybit.wms.entity.User;
import com.sleepybit.wms.entity.UserRole;
import com.sleepybit.wms.service.UserService;
import com.sleepybit.wms.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user/queryUser", method = RequestMethod.POST)
    public List<User> queryUser(@RequestBody User user) {
        return userService.queryUser(user);
    }

    @RequestMapping(value = "/user/addUser", method = RequestMethod.POST)
    public Integer addUser(@RequestBody User user) throws NoSuchAlgorithmException {
        return userService.addUser(user);
    }

    @RequestMapping(value = "/user/editUser", method = RequestMethod.POST)
    public Integer editUser(@RequestBody User user) {
        return userService.editUser(user);
    }

    @RequestMapping(value = "/user/deleteUser", method = RequestMethod.POST)
    public Integer deleteUser(@RequestBody User user) {
        return userService.deleteUser(user);
    }

    @RequestMapping(value = "/user/resetUser", method = RequestMethod.POST)
    public Integer resetUser(@RequestBody User user) {
        try {
            user.setPassword(MD5Util.crypt("sleepybit"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return userService.editUser(user);
    }

    @RequestMapping(value = "/user/userRole", method = RequestMethod.POST)
    public Integer userRole(@RequestBody List<UserRole> userRoles) {
        return userService.userRole(userRoles);
    }

}
