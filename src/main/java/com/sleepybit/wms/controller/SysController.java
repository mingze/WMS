package com.sleepybit.wms.controller;

import com.sleepybit.wms.entity.HomeInfo;
import com.sleepybit.wms.entity.Permit;
import com.sleepybit.wms.entity.User;
import com.sleepybit.wms.service.SysService;
import com.sleepybit.wms.util.MD5Util;
import org.apache.ibatis.ognl.IntHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class SysController {

    @Autowired
    private SysService sysService;

    @RequestMapping(value = "/AjaxLogin", method = RequestMethod.POST)
    public User login(@RequestBody User user) throws Exception {
        return sysService.login(user);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout(User user) {
        return;
    }

    @RequestMapping(value = "/getHomeInfo", method = RequestMethod.GET)
    public HomeInfo getHomeInfo() {
        return sysService.getHomeInfo();
    }

    @RequestMapping("getMD5")
    public String getMD5(@RequestBody String s) throws NoSuchAlgorithmException {
        return MD5Util.crypt(s);
    }

    @RequestMapping(value = "/getPermit", method = RequestMethod.GET)
    public List<Permit> getPermit() {
        return sysService.getPermit();
    }

    @RequestMapping(value = "/changePwd", method = RequestMethod.POST)
    public Integer changePwd(@RequestBody User user) {
        return sysService.changePwd(user);
    }

}
