package com.sleepybit.wms.controller;

import com.sleepybit.wms.bo.InOutBO;
import com.sleepybit.wms.bo.ItemBO;
import com.sleepybit.wms.dto.InOutDTO;
import com.sleepybit.wms.dto.ItemDTO;
import com.sleepybit.wms.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemController {

    @Autowired
    ItemService itemService;

    @RequestMapping(value = "/item/listItem", method = RequestMethod.POST)
    public List<ItemDTO> listItem(@RequestBody ItemBO itemBO) {
        return itemService.listItem(itemBO);
    }

    @RequestMapping(value = "/item/getInOutList", method = RequestMethod.POST)
    public List<InOutDTO> getInOutList(@RequestBody InOutBO inOutBO) {
        return itemService.getInOutList(inOutBO);
    }
}
