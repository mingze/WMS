package com.sleepybit.wms.controller;

import com.sleepybit.wms.entity.Role;
import com.sleepybit.wms.entity.RolePermit;
import com.sleepybit.wms.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/role/queryRole", method = RequestMethod.POST)
    public List<Role> queryRole(@RequestBody Role role) {
        return roleService.queryRole(role);
    }

    @RequestMapping(value = "/role/addRole", method = RequestMethod.POST)
    public Integer addRole(@RequestBody Role role) {
        return roleService.addRole(role);
    }

    @RequestMapping(value = "/role/editRole", method = RequestMethod.POST)
    public Integer editRole(@RequestBody Role role) {
        return roleService.editRole(role);
    }

    @RequestMapping(value = "/role/deleteRole", method = RequestMethod.POST)
    public Integer deleteRole(@RequestBody Role role) {
        return roleService.deleteRole(role);
    }

    @RequestMapping(value = "/role/rolePermit", method = RequestMethod.POST)
    public Integer rolePermit(@RequestBody List<RolePermit> rolePermits) {
        return roleService.rolePermit(rolePermits);
    }

}
