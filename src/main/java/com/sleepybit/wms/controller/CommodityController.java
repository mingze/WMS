package com.sleepybit.wms.controller;

import com.sleepybit.wms.bo.BrandBO;
import com.sleepybit.wms.bo.CommodityBO;
import com.sleepybit.wms.bo.KindBO;
import com.sleepybit.wms.entity.Brand;
import com.sleepybit.wms.entity.Commodity;
import com.sleepybit.wms.entity.Kind;
import com.sleepybit.wms.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CommodityController {

    @Autowired
    CommodityService commodityService;

    @RequestMapping(value = "/commodity/getBrandList", method = RequestMethod.POST)
    public List<Brand> getBrandList(@RequestBody BrandBO brandBO) {
        return commodityService.getBrandList(brandBO);
    }

    @RequestMapping(value = "/commodity/addBrand", method = RequestMethod.POST)
    public Integer insertBrand(@RequestBody Brand brand) {
        return commodityService.insertBrand(brand);
    }

    @RequestMapping(value = "/commodity/updateBrand", method = RequestMethod.POST)
    public Integer updateBrand(@RequestBody Brand brand) {
        return commodityService.updateBrand(brand);
    }

    @RequestMapping(value = "/commodity/deleteBrand", method = RequestMethod.POST)
    public Integer deleteBrand(@RequestBody List<Brand> brands) {
        return commodityService.deleteBrand(brands);
    }

    @RequestMapping(value = "/commodity/getKindList", method = RequestMethod.POST)
    public List<Kind> getKindList(@RequestBody KindBO kindBO) {
        return commodityService.getKindList(kindBO);
    }

    @RequestMapping(value = "/commodity/addKind", method = RequestMethod.POST)
    public Integer insertKind(@RequestBody Kind kind) {
        return commodityService.insertKind(kind);
    }

    @RequestMapping(value = "/commodity/updateKind", method = RequestMethod.POST)
    public Integer updateKind(@RequestBody Kind kind) {
        return commodityService.updateKind(kind);
    }

    @RequestMapping(value = "/commodity/deleteKind", method = RequestMethod.POST)
    public Integer deleteKind(@RequestBody List<Kind> kinds) {
        return commodityService.deleteKind(kinds);
    }

    @RequestMapping(value = "/commodity/getCommodityList", method = RequestMethod.POST)
    public List<Commodity> getCommodityList(@RequestBody CommodityBO commodityBO) {
        return commodityService.getCommodityList(commodityBO);
    }

    @RequestMapping(value = "/commodity/addCommodity", method = RequestMethod.POST)
    public Integer insertCommodity(@RequestBody Commodity commodity) {
        return commodityService.insertCommodity(commodity);
    }

    @RequestMapping(value = "/commodity/updateCommodity", method = RequestMethod.POST)
    public Integer updateCommodity(@RequestBody Commodity commodity) {
        return commodityService.updateCommodity(commodity);
    }

    @RequestMapping(value = "/commodity/deleteCommodity", method = RequestMethod.POST)
    public Integer deleteCommodity(@RequestBody List<Commodity> commodities) {
        return commodityService.deleteCommodity(commodities);
    }

    @RequestMapping(value = "/commodity/getBrandByName", method = RequestMethod.POST)
    public List<Brand> getBrandByName(@RequestBody Brand brand) {
        return commodityService.getBrandByName(brand);
    }
}
