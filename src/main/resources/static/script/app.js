'use strict';

var app = angular.module('app', [
    "ngAnimate",
    "ui.bootstrap",
    'ui.grid',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ui.grid.autoResize',
    'ui.grid.edit',
    'ui.grid.exporter',
    'ui.grid.pagination',
    'ui.grid.cellNav',
    "ui.grid.grouping",
    "ui.grid.emptyBaseLayer",
    "chart.js",
    'ui.grid.exporter',
    "ngRoute"

]);

// app.config(function($routeProvider, $locationProvider) {
//     $routeProvider
//         .when('/logout', {
//             templateUrl: 'book.html',
//             controller: 'BookController',
//             resolve: {
//                 // I will cause a 1 second delay
//                 delay: function($q, $timeout) {
//                     var delay = $q.defer();
//                     $timeout(delay.resolve, 1000);
//                     return delay.promise;
//                 }
//             }
//         })
//         .when('/Book/:bookId/ch/:chapterId', {
//             templateUrl: 'chapter.html',
//             controller: 'ChapterController'
//         });
//
//     // configure html5 to get links working on jsfiddle
//     $locationProvider.html5Mode(true);
// });




