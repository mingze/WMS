app.controller('appCtr', ['$rootScope', '$scope', "$http", "$uibModal", function ($rootScope, $scope, $http, $uibModal) {
    $scope.header = {};
    $scope.header.mail = 123;

    $scope.content = {};
    $scope.content.titleName = '首页';
    $scope.content.testSrc = 'view/home.html';

    $scope.isLogin = false;
    $scope.user = {};

    $scope.openTab = function (title, fileSrc) {
        $scope.content.titleName = title;
        $scope.content.testSrc = fileSrc;
    };

    $scope.login = function () {
        if ($scope.user.username == null || $scope.user.username == '') {
            Alert.error("请输入用户名");
            return
        }
        if ($scope.user.password == null || $scope.user.password == '') {
            Alert.error("请输入密码");
            return
        }
        $http({
            method: "post",
            url: "/AjaxLogin",
            data: $scope.user
        }).then(function (value) {
            if (value.data.username) {
                $scope.user = value.data;
                $scope.user.permitMap = new Map();
                $scope.user.permitList.forEach(function (item, index) {
                    $scope.user.permitMap[item.code] = item;
                });
                $scope.content.titleName = '首页';
                $scope.content.testSrc = 'view/home.html';
                $scope.isLogin = true;
                console.log($scope.user);
            } else {
                Alert.error("用户名或密码错误！")
            }
        }, function (va) {
            console.log(va);
        })
    };
    $scope.logout = function () {
        $scope.isLogin = false;
        $scope.user = {};
    };
    $scope.openPassModal = function () {
        $scope.pwd = {};
        $scope.passModal = $uibModal.open({
            templateUrl: "layout/passModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.changePwd = function () {
        if ($scope.pwd.pwd1 == null || $scope.pwd.pwd1 == "") {
            Alert.error("密码不能为空");
            return;
        }
        if ($scope.pwd.pwd2 != $scope.pwd.pwd1) {
            Alert.error("密码不一致");
            return;
        } else {
            $scope.pwd.password = $scope.pwd.pwd1;
        }
        if (md5($scope.pwd.oldpwd) != $scope.user.password) {
            Alert.error("原密码错误");
            return;
        }
        let temp = $scope.user.password;
        $scope.user.password = md5($scope.pwd.pwd1);
        $http({
            method: "post",
            url: "/changePwd",
            data: $scope.user
        }).then(function (value) {
            $scope.passModal.close();
            Alert.success("密码修改成功 请重新登录");
            $scope.logout();
        }, function (va) {
            console.log(va);
        })
    }
}]);

