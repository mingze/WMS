app.controller('homeCtr', ['$scope', '$http', function ($scope, $http) {
    $scope.init = function () {
        $http({
            method: "get",
            url: "/getHomeInfo",
        }).then(function (value) {
            var result = value.data;
            $scope.brandNum = result.brand;
            $scope.kindNum = result.kind;
            $scope.commodityNum = result.commodity;
        });
    }
}]);