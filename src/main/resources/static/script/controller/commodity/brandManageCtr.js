app.controller('brandManageCtr', ['$scope', '$http', '$uibModal', 'uiGridConstants', function ($scope, $http, $uibModal, uiGridConstants) {

    $scope.queryParam = {};
    $scope.addParam = {};
    $scope.updateParam = {};
    $scope.brandGird = {
        appScopeProvider: $scope,
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "品牌名称", enableColumnMenu: false},
            // {
            //     name: 'parentName', displayName: "父级品牌名称", enableColumnMenu: false,
            // },
            {
                name: 'description', displayName: "品牌描述", enableColumnMenu: false,
            }
        ],
        paginationPageSizes: [10, 15, 20],
        exporterExcelFilename: 'export.xlsx',
        exporterExcelSheetName: 'Sheet1',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };

    $scope.init = function () {
        $scope.queryBrand();
    };
    $scope.queryBrand = function () {
        $http({
            method: 'POST',
            url: '/commodity/getBrandList',
            data: $scope.queryParam
        }).then(function (value) {
            var result = value.data;
            for (temp in result) {
                for (temp2 in result) {
                    if (result[temp2].id == result[temp].parentId) {
                        result[temp].parentName = result[temp2].name;
                    }
                }
            }
            $scope.brandList = result;
            // console.log($scope.brandList);
            $scope.brandGird.data = $scope.brandList;
        });

    };
    //
    $scope.openAddModal = function () {
        $scope.addModal = $uibModal.open({
            templateUrl: "view/commodityManage/brand/addModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.openEditModal = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
        } else {
            $scope.updateParam = angular.copy(currentSelection[0]);
            $scope.editModal = $uibModal.open({
                templateUrl: "view/commodityManage/brand/passModal.html",
                scope: $scope,
                backdrop: 'static'
            });
        }
    };
    $scope.saveBrand = function () {
        if($scope.addParam.name==null||$scope.addParam.name==""){
            Alert.error("名称不能为空");
            return;
        }
        Alert.confirm("确定要保存吗？", function () {
            var brand = {
                name: $scope.addParam.name,
                parentId: $scope.addParam.parentId,
                description: $scope.addParam.description,
            };
            $http({
                method: 'POST',
                url: '/commodity/addBrand',
                data: brand
            }).then(function (value) {
                Alert.success("添加成功！")
                $scope.addModal.close();
                $scope.queryBrand();
            });
        });
    };
    $scope.updateBrand = function () {
        if($scope.updateParam.name==null||$scope.updateParam.name==""){
            Alert.error("名称不能为空");
            return;
        }
        Alert.confirm("确定要修改吗？", function () {
            var brand = {
                    id: $scope.updateParam.id,
                    name: $scope.updateParam.name,
                    parentId: $scope.updateParam.parentId,
                    description: $scope.updateParam.description
                }
            ;
            $http({
                method: 'POST',
                url: '/commodity/updateBrand',
                data: brand
            }).then(function (value) {
                Alert.success("修改成功！")
                $scope.editModal.close();
                $scope.queryBrand();
            });
        });
    };
    $scope.deleteBrand = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else {
            Alert.confirm("确定要删除吗？", function () {
                $http({
                    method: 'POST',
                    url: '/commodity/deleteBrand',
                    data: currentSelection
                }).then(function (value) {
                    Alert.success("删除成功！")
                    $scope.queryBrand();
                }, function (reason) {
                    Alert.error(reason);
                });

            });
        }
    }

    $scope.export = function () {
        $scope.gridApi.exporter.excelExport('all', 'all');
    }
}]);