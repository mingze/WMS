app.controller('kindManageCtr', ['$scope', '$http', '$uibModal', function ($scope, $http, $uibModal) {

    $scope.queryParam = {};
    $scope.addParam = {};
    $scope.updateParam = {};

    $scope.kindGird = {
        appScopeProvider: $scope,
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "品类名称", enableColumnMenu: false},
            {
                name: 'description', displayName: "品类描述", enableColumnMenu: false,
            }
        ],
        paginationPageSizes: [10, 15, 20],
        exporterExcelFilename: 'export.xlsx',
        exporterExcelSheetName: 'Sheet1',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };
    $scope.init = function () {
        $scope.queryKind();
    };
    $scope.queryKind = function () {
        $http({
            method: 'POST',
            url: '/commodity/getKindList',
            data: $scope.queryParam
        }).then(function (value) {
            var result = value.data;
            for (temp in result) {
                for (temp2 in result) {
                    if (result[temp2].id == result[temp].parentId) {
                        result[temp].parentName = result[temp2].name;
                    }
                }
            }
            $scope.kindList = result;
            $scope.kindGird.data = $scope.kindList;
        });

    };
    //
    $scope.openAddModal = function () {
        $scope.addModal = $uibModal.open({
            templateUrl: "view/commodityManage/kind/addModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.openEditModal = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
        } else {
            $scope.updateParam = angular.copy(currentSelection[0]);
            $scope.editModal = $uibModal.open({
                templateUrl: "view/commodityManage/kind/passModal.html",
                scope: $scope,
                backdrop: 'static'
            });
        }
    };
    $scope.saveKind = function () {
        if ($scope.addParam.name == null || $scope.addParam.name == '') {
            Alert.error("名称不能为空！");
            return;
        }
        Alert.confirm("确定要保存吗？", function () {
            var kind = {
                name: $scope.addParam.name,
                parentId: $scope.addParam.parentId,
                description: $scope.addParam.description
            };
            $http({
                method: 'POST',
                url: '/commodity/addKind',
                data: kind
            }).then(function (value) {
                Alert.success("添加成功！")
                $scope.addModal.close();
                $scope.addParam = {};
                $scope.queryKind();
            });
        });
    };
    $scope.updateKind = function () {
        if ($scope.updateParam.name == null || $scope.updateParam.name == '') {
            Alert.error("名称不能为空！");
            return;
        }
        Alert.confirm("确定要修改吗？", function () {
            var kind = {
                    id: $scope.updateParam.id,
                    name: $scope.updateParam.name,
                    parentId: $scope.updateParam.parentId,
                    description: $scope.updateParam.description
                }
            ;
            $http({
                method: 'POST',
                url: '/commodity/updateKind',
                data: kind
            }).then(function (value) {
                Alert.success("修改成功！")
                $scope.editModal.close();
                $scope.queryKind();
            });
        });
    };
    $scope.deleteKind = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else {
            Alert.confirm("确定要删除吗？", function () {
                $http({
                    method: 'POST',
                    url: '/commodity/deleteKind',
                    data: currentSelection
                }).then(function (value) {
                    Alert.success("删除成功！")
                    $scope.queryKind();
                }, function (reason) {
                    Alert.error(reason)
                });
            });
        }
    }
    $scope.export = function () {
        $scope.gridApi.exporter.excelExport('all', 'all');
    }

}]);