app.controller('commodityManageCtr', ['$scope', 'commodityManageService', '$http', 'uiGridConstants', "$uibModal", function ($scope, commodityManageService, $http, uiGridConstants, $uibModal) {

    $scope.queryParam = {};
    $scope.addParam = {};
    $scope.updateParam = {};

    $scope.commodityGird = {
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "商品名称", enableColumnMenu: false,},
            {name: 'brandName', displayName: "品牌名称", enableColumnMenu: false,},
            {name: 'kindName', displayName: "品类名称", enableColumnMenu: false,},
            {name: 'purchasePrice', displayName: "采购价格（元/单位）", enableColumnMenu: false,},
            {name: 'salesPrice', displayName: "销售价格（元/单位）", enableColumnMenu: false,},
            {name: 'unit', displayName: "商品单位", enableColumnMenu: false,},
            {name: 'description', displayName: "商品描述", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        exporterExcelFilename: 'export.xlsx',
        exporterExcelSheetName: 'Sheet1',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }

    };

    $scope.init = function () {
        $scope.queryBrand();
    };
    $scope.queryBrand = function () {
        $http({
            method: 'POST',
            url: '/commodity/getBrandList',
            data: {name: null}
        }).then(function (value) {
            var result = value.data;
            // console.log(result);
            $scope.brandList = result;
            // $scope.brandGird.data = $scope.brandList;
            $scope.queryKind();
        });
    };
    $scope.queryKind = function () {
        $http({
            method: 'post',
            url: '/commodity/getKindList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            $scope.kindList = result;
            // $scope.kindGird.data = $scope.kindList;
            $scope.queryCommodity();
        });
    };
    $scope.queryCommodity = function () {
        $http({
            method: 'POST',
            url: '/commodity/getCommodityList',
            data: $scope.queryParam
        }).then(function (value) {
            var result = value.data;
            for (temp in result) {
                for (temp2 in $scope.brandList) {
                    if (result[temp].brandId == $scope.brandList[temp2].id) {
                        result[temp].brandName = $scope.brandList[temp2].name;
                        break;
                    }
                }
                for (temp3 in $scope.brandList) {
                    if (result[temp].kindId == $scope.kindList[temp3].id) {
                        result[temp].kindName = $scope.kindList[temp3].name;
                        break;
                    }
                }
            }
            $scope.commodityList = result;
            // console.log(result);
            $scope.commodityGird.data = $scope.commodityList;
        });
    };
    $scope.openAddModal = function () {
        $scope.addModal = $uibModal.open({
            templateUrl: "view/commodityManage/commodity/addModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.saveCommodity = function () {
        if( $scope.addParam.name==null|| $scope.addParam.name==''){
            Alert.error("名称不能为空！");
            return;
        }
        if( $scope.addParam.brandId==null|| $scope.addParam.brandId==''){
            Alert.error("品牌不能为空！");
            return;
        }
        if( $scope.addParam.kindId==null|| $scope.addParam.kindId==''){
            Alert.error("品类不能为空！");
            return;
        }
        if( $scope.addParam.purchasePrice==null|| $scope.addParam.purchasePrice==''){
            Alert.error("采购价格不能为空！");
            return;
        }
        if( $scope.addParam.salesPrice==null|| $scope.addParam.salesPrice==''){
            Alert.error("销售价格不能为空！");
            return;
        }
        if( $scope.addParam.unit==null|| $scope.addParam.unit==''){
            Alert.error("单位不能为空！");
            return;
        }
        Alert.confirm("确定要保存吗？", function () {
            var commodity = {
                    name: $scope.addParam.name,
                    brandId: $scope.addParam.brandId,
                    kindId: $scope.addParam.kindId,
                    purchasePrice: $scope.addParam.purchasePrice,
                    salesPrice: $scope.addParam.salesPrice,
                    unit: $scope.addParam.unit,
                    description: $scope.addParam.description
                }
            ;
            $http({
                method: 'POST',
                url: '/commodity/addCommodity',
                data: commodity
            }).then(function (value) {
                Alert.success("添加成功！")
                $scope.addModal.close();
                $scope.addParam = {};
                $scope.queryCommodity();
            });
        });
    };
    $scope.openEditModal = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
        } else {
            $scope.updateParam =  $scope.updateParam = angular.copy(currentSelection[0]);;
            $scope.editModal = $uibModal.open({
                templateUrl: "view/commodityManage/commodity/passModal.html",
                scope: $scope,
                backdrop: 'static'
            });
        }

    };
    $scope.updateCommodity = function () {
        if( $scope.updateParam.name==null|| $scope.updateParam.name==''){
            Alert.error("名称不能为空！");
            return;
        }
        if( $scope.updateParam.brandId==null|| $scope.updateParam.brandId==''){
            Alert.error("品牌不能为空！");
            return;
        }
        if( $scope.updateParam.kindId==null|| $scope.updateParam.kindId==''){
            Alert.error("品类不能为空！");
            return;
        }
        if( $scope.updateParam.purchasePrice==null|| $scope.updateParam.purchasePrice==''){
            Alert.error("采购价格不能为空！");
            return;
        }
        if( $scope.updateParam.salesPrice==null|| $scope.updateParam.salesPrice==''){
            Alert.error("销售价格不能为空！");
            return;
        }
        if( $scope.updateParam.unit==null|| $scope.updateParam.unit==''){
            Alert.error("单位不能为空！");
            return;
        }
        Alert.confirm("确定要修改吗？", function () {
            var commodity = {
                id: $scope.updateParam.id,
                name: $scope.updateParam.name,
                brandId: $scope.updateParam.brandId,
                kindId: $scope.updateParam.kindId,
                purchasePrice: $scope.updateParam.purchasePrice,
                salesPrice: $scope.updateParam.salesPrice,
                unit: $scope.updateParam.unit
            };
            $http({
                method: 'POST',
                url: '/commodity/updateCommodity',
                data: commodity
            }).then(function (value) {
                Alert.success("修改成功！")
                $scope.editModal.close();
                $scope.queryCommodity();
            });
        });
    };
    $scope.deleteCommodity = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else {
            Alert.confirm("确定要删除吗？", function () {
                $http({
                    method: 'POST',
                    url: '/commodity/deleteCommodity',
                    data: currentSelection
                }).then(function (value) {
                    Alert.success("删除成功！")
                    $scope.queryCommodity();
                }, function (reason) {
                    Alert.error(reason)
                });
            });
        }
    }
    $scope.export = function () {
        $scope.gridApi.exporter.excelExport('all', 'all');
    }
}]);