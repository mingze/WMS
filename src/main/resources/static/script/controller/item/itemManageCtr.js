app.controller('itemManageCtr', ['$scope', '$http', '$uibModal', function ($scope, $http, $uibModal) {
    $scope.queryParam = {};
    $scope.init = function () {
        $scope.queryBrand();
        $scope.queryItem();
    };
    $scope.queryBrand = function () {
        $http({
            method: 'post',
            url: '/commodity/getBrandList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            $scope.brandList = result;
            $scope.queryKind();
        });
    };
    $scope.queryKind = function () {
        $http({
            method: 'post',
            url: '/commodity/getKindList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            $scope.kindList = result;
            $scope.queryCommodity();
        });
    };
    $scope.queryCommodity = function () {
        $http({
            method: 'post',
            url: '/commodity/getCommodityList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            for (temp in result) {
                for (temp2 in $scope.brandList) {
                    if (result[temp].brandId == $scope.brandList[temp2].id) {
                        result[temp].brandName = $scope.brandList[temp2].name;
                        break;
                    }
                }
                for (temp3 in $scope.brandList) {
                    if (result[temp].kindId == $scope.kindList[temp3].id) {
                        result[temp].kindName = $scope.kindList[temp3].name;
                        break;
                    }
                }
            }
            $scope.commodityList = result;
        });
    };
    $scope.queryItem = function () {
        $http({
            method: 'post',
            url: '/item/listItem',
            data: $scope.queryParam
        }).then(function (value) {
            var result = {};
            result = value.data;
            $scope.itemList = result;
            $scope.itemGird.data = $scope.itemList;
        })
    };
    $scope.itemGird = {
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'commodityName', displayName: "商品名称", enableColumnMenu: false,},
            {name: 'brandName', displayName: "品牌名称", enableColumnMenu: false,},
            {name: 'kindName', displayName: "品类名称", enableColumnMenu: false,},
            {name: 'quantity', displayName: "库存量", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        exporterExcelFilename: 'export.xlsx',
        exporterExcelSheetName: 'Sheet1',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    }
    $scope.export = function () {
        $scope.gridApi.exporter.excelExport('all','all');
    }
}]);