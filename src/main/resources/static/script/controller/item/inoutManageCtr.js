app.controller('inoutManageCtr', ['$scope', '$http', '$uibModal', function ($scope, $http, $uibModal) {
    $scope.queryParam = {};
    $scope.inoutGird = {
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'id', displayName: "出入库流水号", enableColumnMenu: false,},
            {name: 'time', displayName: "时间", enableColumnMenu: false,},
            {name: 'type', displayName: "出/入库", enableColumnMenu: false,},
            {name: 'kindName', displayName: "品类名称", enableColumnMenu: false,},
            {name: 'brandName', displayName: "品牌名称", enableColumnMenu: false,},
            {name: 'commodityName', displayName: "商品名称", enableColumnMenu: false,},
            {name: 'quantity', displayName: "数量", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        exporterExcelFilename: 'export.xlsx',
        exporterExcelSheetName: 'Sheet1',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };

    $scope.init = function () {
        $scope.queryBrand();
        $scope.queryInout();
    };
    $scope.queryBrand = function () {
        $http({
            method: 'post',
            url: '/commodity/getBrandList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            $scope.brandList = result;
            $scope.queryKind();
        });
    };
    $scope.queryKind = function () {
        $http({
            method: 'post',
            url: '/commodity/getKindList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            $scope.kindList = result;
            $scope.queryCommodity();
        });
    };
    $scope.queryCommodity = function () {
        $http({
            method: 'post',
            url: '/commodity/getCommodityList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            for (temp in result) {
                for (temp2 in $scope.brandList) {
                    if (result[temp].brandId == $scope.brandList[temp2].id) {
                        result[temp].brandName = $scope.brandList[temp2].name;
                        break;
                    }
                }
                for (temp3 in $scope.brandList) {
                    if (result[temp].kindId == $scope.kindList[temp3].id) {
                        result[temp].kindName = $scope.kindList[temp3].name;
                        break;
                    }
                }
            }
            $scope.commodityList = result;
        });
    };

    $scope.queryInout = function () {
        $http({
            method: 'post',
            url: '/item/getInOutList',
            data: $scope.queryParam
        }).then(function (value) {
            $scope.inoutList = value.data;
            $scope.inoutGird.data = $scope.inoutList;
        })
    };
    $scope.export = function () {
        $scope.gridApi.exporter.excelExport('all','all');
    }
}]);