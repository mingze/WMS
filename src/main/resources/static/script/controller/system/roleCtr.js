app.controller('roleCtr', ['$scope', "$uibModal", "$http", function ($scope, $uibModal, $http) {
    $scope.init = function () {
        $scope.queryRole();
    };
    $scope.queryParam = {};
    $scope.roleList = [];
    $scope.roleGird = {
        enableSorting: false,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "角色名称", enableColumnMenu: false,},
            {name: 'description', displayName: "角色描述", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };
    $scope.queryRole = function () {
        $http({
            method: "post",
            url: "/role/queryRole",
            data: $scope.queryParam
        }).then(function (value) {
            $scope.roleList = value.data;
            $scope.roleGird.data = $scope.roleList;
        })
    };
    $scope.addRole = function () {
        $scope.addParam = {};
        $scope.addModal = $uibModal.open({
            templateUrl: "view/sysManage/role/addModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.saveRole = function () {
        if ($scope.addParam.name == null) {
            return
        }
        $http({
            method: "post",
            url: "/role/addRole",
            data: $scope.addParam
        }).then(function (value) {
            Alert.success("成功");
            $scope.queryRole();
            $scope.addModal.close();
        }, function () {
            Alert.error("失败")
        })
    }
    $scope.editRole = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
            return
        }
        $scope.updateParam = angular.copy(currentSelection[0]);
        $scope.editModal = $uibModal.open({
            templateUrl: "view/sysManage/role/passModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    }
    $scope.updateRole = function () {
        $http({
            method: "post",
            url: "/role/editRole",
            data: $scope.updateParam
        }).then(function (value) {
            Alert.success("成功");
            $scope.queryRole();
            $scope.editModal.close();
        }, function () {
            Alert.error("失败")
        })
    }
    $scope.deleteRole = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return;
        }
        if (currentSelection.length > 1) {
            Alert.error("该操作不允许多选！");
            return;
        }

        Alert.confirm("确定要删除吗？", function () {
            $http({
                method: 'POST',
                url: '/role/deleteRole',
                data: currentSelection[0]
            }).then(function (value) {
                Alert.success("删除成功！")
                $scope.queryRole();
            }, function (reason) {
                Alert.error(reason.message);
            });

        });

    };
    $scope.permitGird = {
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "权限名称", enableColumnMenu: false,},
            {name: 'code', displayName: "权限编码", enableColumnMenu: false,},
            {name: 'description', displayName: "权限描述", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        onRegisterApi: function (gridApi) {
            $scope.gridApiPermit = gridApi;
        },
        data: $scope.user.permitList
    };
    $scope.rolePermit = function () {
        let currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return;
        }
        if (currentSelection.length > 1) {
            Alert.error("该操作不允许多选！");
            return;
        }
        $scope.permitModal = $uibModal.open({
            templateUrl: "view/sysManage/role/rolePermit.html",
            scope: $scope,
            backdrop: 'static'
        });
    }
    $scope.savePermit = function () {
        let role = $scope.gridApi.selection.getSelectedRows();
        let roleId = role[0].id;
        let permits = $scope.gridApiPermit.selection.getSelectedRows();
        let rolePermit = [];
        for (let i = 0; i < permits.length; i++) {
            rolePermit.push({
                roleId: roleId,
                permitId: permits[i].id
            })
        }
        $http({
            method: 'POST',
            url: '/role/rolePermit',
            data: rolePermit
        }).then(function (value) {
            Alert.success("权限关联成功");
            $scope.permitModal.close();
        }, function (value) {
            console.log(value);
            Alert.error(value)
        });
    }


}]);