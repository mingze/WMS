app.controller('userCtr', ['$scope', "$uibModal", "$http", function ($scope, $uibModal, $http) {
    $scope.init = function () {
        $scope.queryUser();
    };
    $scope.queryParam = {};
    $scope.userList = [];
    $scope.roleList = [];
    $scope.userGird = {
        enableSorting: false,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "用户名称", enableColumnMenu: false,},
            {name: 'username', displayName: "用户账号", enableColumnMenu: false,},
            {name: 'description', displayName: "用户描述", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };
    $scope.queryUser = function () {
        $http({
            method: 'POST',
            url: '/user/queryUser',
            data: $scope.queryParam
        }).then(function (value) {
            $scope.userList = value.data;
            $scope.userGird.data = $scope.userList;
        });
    };

    $scope.addUser = function () {
        $scope.addParam = {};
        $scope.addModal = $uibModal.open({
            templateUrl: "view/sysManage/user/addModal.html",
            scope: $scope,
            backdrop: 'static'
        });

    };
    $scope.saveUser = function () {
        if ($scope.addParam.name == null || $scope.addParam.name == "") {
            Alert.error("名称不能为空");
            return;
        }
        if ($scope.addParam.username == null || $scope.addParam.username == "") {
            Alert.error("账号不能为空");
            return;
        }
        if ($scope.addParam.password1 == null || $scope.addParam.password1 == "") {
            Alert.error("密码不能为空");
            return;
        }
        if ($scope.addParam.password2 != $scope.addParam.password1) {
            Alert.error("密码不能为空");
            return;
        } else {
            $scope.addParam.password = $scope.addParam.password1;
        }
        $http({
            method: 'POST',
            url: '/user/addUser',
            data: $scope.addParam
        }).then(function (value) {
            Alert.success("添加成功！")
            $scope.addModal.close();
            $scope.queryUser();
        });
    };
    $scope.editUser = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
            return
        }
        $scope.updateParam = angular.copy(currentSelection[0]);
        $scope.editModal = $uibModal.open({
            templateUrl: "view/sysManage/user/passModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.updateUser = function () {
        if ($scope.addParam.name == null || $scope.addParam.name == "") {
            Alert.error("名称不能为空");
            return;
        }
        if ($scope.addParam.username == null || $scope.addParam.username == "") {
            Alert.error("登录账号不能为空");
            return;
        }
        $http({
            method: 'POST',
            url: '/user/editUser',
            data: $scope.editParam
        }).then(function (value) {
            Alert.success("添加成功！")
            $scope.editModal.close();
            $scope.queryUser();
        });
    };

    $scope.deleteUser = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
            return
        }
        Alert.confirm("确定要删除吗？", function () {
            $http({
                method: 'POST',
                url: '/user/deleteUser',
                data: currentSelection[0]
            }).then(function (value) {
                Alert.success("删除成功！")
                $scope.queryUser();
            });
        })

    };

    $scope.resetUser = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
            return
        }
        Alert.confirm("确定重置用户密码吗？", function () {
            $http({
                method: 'POST',
                url: '/user/resetUser',
                data: currentSelection[0]
            }).then(function (value) {
                Alert.success("密码已经重置为“sleepybit”！")
                $scope.queryUser();
            });
        })
    }

    $scope.queryRole = function () {
        $http({
            method: "post",
            url: "/role/queryRole",
            data: {}
        }).then(function (value) {
            $scope.roleList = value.data;
            $scope.roleGird.data = $scope.roleList;
        })
    };
    $scope.roleGird = {
        enableSorting: false,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "权限名称", enableColumnMenu: false,},
            {name: 'code', displayName: "权限编码", enableColumnMenu: false,},
            {name: 'description', displayName: "权限描述", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        onRegisterApi: function (gridApi) {
            $scope.gridApiRole = gridApi;
        },
    };
    $scope.userRole = function () {
        let currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return;
        }
        if (currentSelection.length > 1) {
            Alert.error("该操作不允许多选！");
            return;
        }
        $scope.queryRole();
        $scope.permitModal = $uibModal.open({
            templateUrl: "view/sysManage/user/userRole.html",
            scope: $scope,
            backdrop: 'static'
        });
    }
    $scope.saveRole = function () {
        let user = $scope.gridApi.selection.getSelectedRows();
        let uid = user[0].id;
        let roles = $scope.gridApiRole.selection.getSelectedRows();
        let userRole = [];
        for (let i = 0; i < roles.length; i++) {
            userRole.push({
                uid: uid,
                roleId: roles[i].id
            })
        }
        $http({
            method: 'POST',
            url: '/user/userRole',
            data: userRole
        }).then(function (value) {
            Alert.success("角色关联成功");
            $scope.permitModal.close();
        }, function (value) {
            console.log(value);
            Alert.error(value)
        });
    }

}]);