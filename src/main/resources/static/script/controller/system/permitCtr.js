app.controller('permitCtr', ['$scope', function ($scope) {
    $scope.permitList = [];
    $scope.permitGird = {
        enableSorting: false,
        enableRowSelection: true,
        columnDefs: [
            {name: 'name', displayName: "权限名称", enableColumnMenu: false,},
            {name: 'code', displayName: "权限编码", enableColumnMenu: false,},
            {name: 'description', displayName: "权限描述", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        data: $scope.user.permitList
    }
}]);