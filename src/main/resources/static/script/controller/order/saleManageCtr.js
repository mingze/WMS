app.controller('saleManageCtr', ['$scope', '$http', '$uibModal', function ($scope, $http, $uibModal) {
    $scope.queryParam = {};
    $scope.amount = 0;
    $scope.commodityList = [];
    $scope.add = function () {
        $scope.DATA.data.push({});
    };
    $scope.delete = function ($index) {
        $scope.DATA.data.splice($index, 1);
    };
    $scope.orderGird = {
        enableSorting: false,
        enableRowSelection: true,
        columnDefs: [
            {name: 'id', displayName: "出库单号", enableColumnMenu: false,},
            {name: 'time', displayName: "时间", enableColumnMenu: false,},
            {name: 'amount', displayName: "出货金额（元）", enableColumnMenu: false,},
            {name: 'status', displayName: "状态", enableColumnMenu: false,},
        ],
        paginationPageSizes: [10, 15, 20],
        exporterExcelFilename: 'export.xlsx',
        exporterExcelSheetName: 'Sheet1',
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };
    $scope.orderGird.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
    };

    $scope.queryCommodity = function () {
        $http({
            method: 'post',
            url: '/commodity/getCommodityList',
            data: {}
        }).then(function (value) {
            var result = value.data;
            $scope.commodityList = result;
        });
    };
    $scope.querySalesOrder = function () {
        $http({
            method: "post",
            url: "/order/getSalesOrderList",
            data: $scope.queryParam
        }).then(function (value) {
            var result = value.data;
            $scope.orderGird.data = result;
        })
    };
    $scope.openAddModal = function () {
        $scope.DATA = new Object();
        $scope.DATA.data = [{}];
        $scope.addParam = [];
        $scope.addModal = $uibModal.open({
            templateUrl: "view/orderManage/sales/addModal.html",
            scope: $scope,
            backdrop: 'static',
        });
    };
    $scope.closeAddModal = function () {
        $scope.addModal.close();
        $scope.querySalesOrder();
    };
    $scope.createSalesOrder = function () {
        var result = $scope.DATA.data;
        $scope.addParam = $scope.DATA.data;
        // for (i in result)
        var temp = 0;
        for (i in result) {
            if (result[i].commodity != null && result[i].quantity != null) {
                temp += result[i].commodity.salesPrice * result[i].quantity;
            } else {
                Alert.error("存在非法输入");
                temp = 0;
                return;
            }
        }
        var str = "订单总额为" + temp + "元";
        Alert.confirm(str, function () {
            $http({
                method: "post",
                url: "/order/createSalesOrder",
                data: $scope.addParam
            }).then(function (value) {
                Alert.success("成功");
                $scope.closeAddModal();
            })
        })
    };

    $scope.openEditModal = function () {
        $scope.DATA = new Object();
        $scope.DATA.data = [];
        $scope.editParam = {};
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
        } else if (currentSelection[0].status == "已通过") {
            Alert.error("已通过订单禁止修改！")
        } else {
            // $scope.updateParam = currentSelection[0];
            var orderId = currentSelection[0].id;
            $http({
                method: "post",
                url: "/order/getOrderItem",
                data: orderId
            }).then(function (value) {
                var result = value.data;
                for (i in result) {
                    for (j in $scope.commodityList) {
                        if (result[i].commodityId == $scope.commodityList[j].id) {
                            $scope.DATA.data.push({
                                commodity: $scope.commodityList[j],
                                commodityId: $scope.commodityList[j].id,
                                quantity: result[i].quantity,
                                orderId: orderId
                            });
                            break;
                        }
                    }
                }
            });
            $scope.editModal = $uibModal.open({
                templateUrl: "view/orderManage/sales/passModal.html",
                scope: $scope,
                backdrop: 'static'
            });
        }
    };
    $scope.closeEditModal = function () {
        $scope.editModal.close();
        $scope.querySalesOrder();
    }
    $scope.editSalesOrder = function () {
        var result = $scope.DATA.data;
        $scope.editParam = $scope.DATA.data;
        // for (i in result)
        var temp = 0;
        for (i in result) {
            if (result[i].commodity != null && result[i].quantity != null) {
                temp += result[i].commodity.salesPrice * result[i].quantity;
            } else {
                Alert.error("存在非法输入");
                temp = 0;
                return;
            }
        }
        var str = "订单总额为" + temp + "元";
        Alert.confirm(str, function () {
            $http({
                method: "post",
                url: "/order/editSalesOrder",
                data: $scope.editParam
            }).then(function (value) {
                Alert.success("成功");
                $scope.closeEditModal();
            })
        })
    };
    $scope.orderItemGird = {
        enableSorting: true,
        enableRowSelection: true,
        columnDefs: [
            {name: 'commodityName', displayName: "商品名称", enableColumnMenu: false,},
            {name: 'quantity', displayName: "出库数量", enableColumnMenu: false,},
            {name: 'unit', displayName: "单位", enableColumnMenu: false,},
        ],
        data: []
    };
    $scope.openCheckModal = function () {
        $scope.orderItemGird.data = [];
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
        } else if (currentSelection[0].status == "已通过") {
            Alert.info("该订单已审核通过！")
        } else {
            var orderId = currentSelection[0].id;
            $http({
                method: "post",
                url: "/order/getOrderItem",
                data: orderId
            }).then(function (value) {
                var result = value.data;
                for (i in result) {
                    for (j in $scope.commodityList) {
                        if (result[i].commodityId == $scope.commodityList[j].id) {
                            $scope.orderItemGird.data.push({
                                commodityName: $scope.commodityList[j].name,
                                quantity: result[i].quantity,
                                unit: $scope.commodityList[j].unit
                            });
                            break;
                        }
                    }
                }
            });
            $scope.checkModal = $uibModal.open({
                templateUrl: "view/orderManage/sales/checkModal.html",
                scope: $scope,
                backdrop: 'static'
            });
        }
    };
    $scope.closeCheckModal = function () {
        $scope.checkModal.close();
        $scope.querySalesOrder();
    };
    $scope.pass = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        var orderId = currentSelection[0].id;
        Alert.confirm("审核通过后订单禁止修改！", function () {
            $http({
                method: "post",
                url: "/order/passOrder",
                data: orderId
            }).then(function (value) {
                console.log(value);
                if (value.data == -2) {
                    Alert.error("当前库存不足,无法完成订单");
                    $scope.closeCheckModal();
                    return;
                } else {
                    Alert.success("订单已通过");
                    $scope.closeCheckModal();
                    return;
                }
            })
        })
    };
    $scope.refuse = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        var orderId = currentSelection[0].id;
        $http({
            method: "post",
            url: "/order/refuseOrder",
            data: orderId
        }).then(function (value) {
            Alert.success("订单已拒绝");
            $scope.closeCheckModal();
        })
    };
    $scope.deleteOrder = function () {
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
        } else if (currentSelection.length > 1) {
            Alert.error("该操作不可多选！");
        } else if (currentSelection[0].status == "已通过") {
            Alert.error("已通过订单禁止删除！")
        }
        else {
            var orderId = currentSelection[0].id;
            Alert.confirm("确定要删除吗？", function () {
                $http({
                    method: "post",
                    url: "/order/deleteOrder",
                    data: orderId
                }).then(function (value) {
                    Alert.success("成功")
                    $scope.querySalesOrder();
                });
            })

        }
    };
    $scope.init = function () {
        $scope.queryCommodity();
        $scope.querySalesOrder();
    }
    $scope.export = function () {
        $scope.gridApi.exporter.excelExport('all', 'all');
    }

    $scope.openDetailModal = function () {
        $scope.orderItemGird.data = [];
        var currentSelection = $scope.gridApi.selection.getSelectedRows();
        if (currentSelection.length == 0) {
            Alert.error("请选择数据！");
            return;
        }
        var orderId = currentSelection[0].id;
        $http({
            method: "post",
            url: "/order/getOrderItem",
            data: orderId
        }).then(function (value) {
            var result = value.data;
            for (i in result) {
                for (j in $scope.commodityList) {
                    if (result[i].commodityId == $scope.commodityList[j].id) {
                        $scope.orderItemGird.data.push({
                            commodityName: $scope.commodityList[j].name,
                            quantity: result[i].quantity,
                            unit: $scope.commodityList[j].unit
                        });
                        break;
                    }
                }
            }
        });
        $scope.detailModal = $uibModal.open({
            templateUrl: "view/orderManage/sales/detailModal.html",
            scope: $scope,
            backdrop: 'static'
        });
    };
    $scope.closeDetailModal = function () {
        $scope.detailModal.close();
    }
}]);