function AjaxHttp(url, data, success,ajaxConfig) {
    var self = this;
    this.url = url;
    //this.data = data ? JSON.stringify(data) : null;
    this.success = success;
    var config = {
        url: self.url,
        //type: 'POST',
        data: self.data,
        contentType: "application/json;charset=utf-8",
        dataType: 'json',
        async: false,
        success: function (response) {
            if (self.success) {
                self.success(response);
            } else {
                Alert.error(response.msg);
            }
        }
    };
    $.extend(config, ajaxConfig);

    this.post = function () {
        config.type = "POST";
        if(!config.stringify){
            config.stringify=true;
        }
        if(config.stringify){
            config.data=data ? JSON.stringify(data) : null;
        }else{
            config.data=data;
        }
        $.ajax(config);
    };
    this.get = function () {
        config.type = "GET";
        if(!config.stringify){
            config.stringify=false;
        }
        if(config.stringify){
            config.data=data ? JSON.stringify(data) : null;
        }else{
            config.data=data;
        }
        $.ajax(config);
    };
    this.del = function () {
        config.type = "DELETE";
        if(!config.stringify){
            config.stringify=true;
        }
        if(config.stringify){
            config.data=data ? JSON.stringify(data) : null;
        }else{
            config.data=data;
        }
        $.ajax(config);
    };
    this.put = function () {
        config.type = "PUT";
        if(!config.stringify){
            config.stringify=true;
        }
        if(config.stringify){
            config.data=data ? JSON.stringify(data) : null;
        }else{
            config.data=data;
        }
        $.ajax(config);
    }

}