
/**
 * 封装提示框。
 * 依赖于 sweatAlert2
 */
function Alert() {
};

/**
 *
 * @param content  提示的内容
 */
Alert.alert = function (content) {
    swal(content);
};

/**
 *
 * @param content  提示的内容
 * @param title    标题，可不填
 */
Alert.success = function (content, title) {
    var param = {
        width: '400px',
        type: 'success',
        timer:3000,
        text: content
    };
    if (title) {
        param.title = title;
    }
    swal(param);
};

/**
 *
 * @param content  提示的内容
 * @param title    标题，可不填
 */
Alert.warning = function (content, title) {
    var param = {
        width: '400px',
        type: 'warning',
        text: content
    };
    if (title) {
        param.title = title;
    }
    swal(param);
};

/**
 *
 * @param content  提示的内容
 * @param title    标题，可不填
 */
Alert.error = function (content, title) {
    var param = {
        width: '400px',
        type: 'error',
        timer:3000,
        text: content
    };

    if (title) {
        param.title = title;
    }

    swal(param);
};

/**
 *
 * @param content         提示的内容
 * @param confirmCallback 确认的回调函数
 * @param cancelCallback  取消的回调函数
 * @param title           标题
 */
Alert.confirm = function (content, confirmCallback, cancelCallback, title) {
    var param = {
        width: '400px',
        type: 'warning',
        text: content,
        showCancelButton: true,
        confirmButtonText: '确认',
        cancelButtonText: '取消',
        reverseButtons: true,
        allowOutsideClick: false
    };
    if (title) {
        param.title = title;
    }
    swal(param).then(function (isConfirm) {
        if (isConfirm && confirmCallback) {
            confirmCallback();
        } else if (!isConfirm && cancelCallback) {
            cancelCallback();
        }
    });
}


/**
 *
 * @param content  提示的内容
 * @param title    标题，可不填
 */
Alert.info = function (content, title) {
    var param = {
        width: '400px',
        type: 'info',
        text: content
    };
    if (title) {
        param.title = title;
    }
    swal(param);
};

/*PromptBox.question = function (content, title) {
 if (title == null || title == undefined || title.length <= 0) {
 title = "问题:";
 }
 swal(title, content, 'question');
 };*/

/**
 * 集合拓展功能
 */
function ArrayExt() {

};
/* 遍历集合查找对应值，从集合中删除对象 */
ArrayExt.arraySplice = function (array, value, field) {
    var flag = false;
    if (field) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][field] == value) {
                flag = true;
                array.splice(i, 1);
                break;
            }
        }
    }
    else {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == value) {
                flag = true;
                array.splice(i, 1);
                break;
            }
        }
    }
    return flag;
};
/* 遍历集合查找对应值，返回值所在对象的索引 */
ArrayExt.arrayIndex = function (array, value, field) {
    if (field) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][field] == value) {
                return i;
            }
        }
    }
    else {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }
    }
    return -1;
};